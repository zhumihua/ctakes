

/* First created by JCasGen Tue Dec 09 23:28:15 EST 2014 */
package org.apache.ctakes.assertion.zoner.types;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** A document Zone, including its heading
 * Updated by JCasGen Tue Dec 09 23:28:15 EST 2014
 * XML source: /Users/lusisi/i2b2/apache-ctakes-3.1.1-src/ctakes-assertion-zoner/src/main/resources/org/apache/ctakes/assertion/zoner/types/TypeSystem.xml
 * @generated */
public class Zone extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Zone.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Zone() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Zone(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Zone(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Zone(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: label

  /** getter for label - gets 
   * @generated */
  public String getLabel() {
    if (Zone_Type.featOkTst && ((Zone_Type)jcasType).casFeat_label == null)
      jcasType.jcas.throwFeatMissing("label", "org.apache.ctakes.assertion.zoner.types.Zone");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Zone_Type)jcasType).casFeatCode_label);}
    
  /** setter for label - sets  
   * @generated */
  public void setLabel(String v) {
    if (Zone_Type.featOkTst && ((Zone_Type)jcasType).casFeat_label == null)
      jcasType.jcas.throwFeatMissing("label", "org.apache.ctakes.assertion.zoner.types.Zone");
    jcasType.ll_cas.ll_setStringValue(addr, ((Zone_Type)jcasType).casFeatCode_label, v);}    
  }

    