#
export MAVEN_OPTS="-Xmx2048m"
cd ../ctakes-core
mvn -Dmaven.test.skip=true install
cd ../ctakes-type-system
mvn -Dmaven.test.skip install
cd ../ctakes-clinical-pipeline
#mvn clean compile -PrunClinicalPipeline -X > debug.txt
#mvn clean compile -PrunGetDataset -DinputXMI=/data/i2b2/2014i2b2/tools/CTAKES/data/output/ -DoutputDS=/data/i2b2/2014i2b2/tools/CTAKES/data/dsOutput/

mvn clean compile -PrunGetDataset -DinputXMI=/data/i2b2/2014i2b2/tools/CTAKES/data/output/ -DoutputDS=/data/i2b2/2014i2b2/tools/CTAKES/data/dsOutput/
