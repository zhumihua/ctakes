package org.apache.ctakes.clinicalpipeline;
/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */


import java.io.File;
import java.util.Date;
import java.util.Iterator;

import org.apache.ctakes.assertion.util.AssertionConst;
import org.apache.ctakes.core.util.CtakesFileNamer;
import org.apache.ctakes.typesystem.type.textspan.Sentence;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.cleartk.util.cr.FilesCollectionReader;
import org.uimafit.component.xwriter.XWriter;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.pipeline.SimplePipeline;
import org.uimafit.factory.JCasFactory;

/**
 * Run the plaintext clinical pipeline, using the dictionary of terms from UMLS.
 * Note you must have the UMLS password supplied in some way - see the 
 * User or Developer Guide for information on options for doing that.
 * Also note you need to have the UMLS dictionaries available (they 
 * are separate download from Apache cTAKES itself due to licensing).
 * 
 * Input and output directory names are taken from {@link AssertionConst} 
 * 
 */
public class ReaderXMIFile {


	private JCas xmiFile;
	
	
	
	
	public static void printSent(String sent){
		System.out.println("`````````````");
		System.out.println(sent);
		System.out.println("`````````````");
	}

	public static void main(String[] args) throws Exception {

		//if(args==null||args.length)
		
		JCas xmiFile=JCasFactory.createJCas();
		JCasFactory.loadJCas(xmiFile, "data/output/7.xmi");
		//JCas plainTextjcas=xmiFile.getView("plaintext");
		
		String text=xmiFile.getDocumentText();
		AnnotationIndex<Annotation> sentAnnot=xmiFile.getAnnotationIndex(Sentence.type);
		Iterator<Annotation> it=sentAnnot.iterator();
		
		while(it.hasNext()){
			Annotation currAnno=it.next();
			int start=currAnno.getBegin();
			int end=currAnno.getEnd();
			String sent=text.substring(start,end);
			printSent(sent);
		}
		
	}


}
