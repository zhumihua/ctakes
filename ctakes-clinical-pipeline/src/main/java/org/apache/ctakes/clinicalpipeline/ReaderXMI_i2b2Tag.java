package org.apache.ctakes.clinicalpipeline;
/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.ctakes.assertion.util.AssertionConst;
import org.apache.ctakes.clinicalpipeline.utils.StanfordParser;
import org.apache.ctakes.clinicalpipeline.utils.StrIndex;
import org.apache.ctakes.clinicalpipeline.utils.TimeLexicons;
import org.apache.ctakes.core.util.CtakesFileNamer;
import org.apache.ctakes.drugner.type.DrugMentionAnnotation;
import org.apache.ctakes.medtime.type.MedTimex3;
import org.apache.ctakes.postagger.POSTagger;
import org.apache.ctakes.typesystem.type.syntax.BaseToken;
import org.apache.ctakes.typesystem.type.syntax.Lemma;
import org.apache.ctakes.typesystem.type.syntax.WordToken;
import org.apache.ctakes.typesystem.type.textspan.Sentence;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.cleartk.util.cr.FilesCollectionReader;
import org.uimafit.component.xwriter.XWriter;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.pipeline.SimplePipeline;
import org.uimafit.util.JCasUtil;
import org.uimafit.factory.JCasFactory;
import org.apache.ctakes.typesystem.type.i2b2.*;


/**
 * Run the plaintext clinical pipeline, using the dictionary of terms from UMLS.
 * Note you must have the UMLS password supplied in some way - see the 
 * User or Developer Guide for information on options for doing that.
 * Also note you need to have the UMLS dictionaries available (they 
 * are separate download from Apache cTAKES itself due to licensing).
 * 
 * Input and output directory names are taken from {@link AssertionConst} 
 * 
 */
public class ReaderXMI_i2b2Tag {


	private JCas xmiFile;
	
	
	
	
	public static void printSent(String sent){
		System.out.println("`````````````");
		System.out.println(sent);
		System.out.println("`````````````");
	}
	
	public static ArrayList<File> listFiles(String dirName){
		File folder=new File(dirName);
		File[] listFiles=folder.listFiles();
		
		ArrayList<File> rets=new ArrayList<File>();
		for(int i=0;i<listFiles.length;i++){
			if(listFiles[i].isFile()&&listFiles[i].getName().endsWith(".xmi")){
				rets.add(listFiles[i]);
			}	
			}
		return rets;
	}


	//posTag
	public static void xmi_tag_verb_sec2(String xmiName,String dirOut) throws Exception{
		
		
		JCas xmiFile=JCasFactory.createJCas();
		JCasFactory.loadJCas(xmiFile, xmiName);
		JCas x= xmiFile.getView("UriView");
		String originFileName=x.getSofaDataURI();
		String[] values=originFileName.split("\\/");
		String fileName=values[values.length-1];
		PrintWriter writer=new PrintWriter(new BufferedWriter(new FileWriter(dirOut+fileName+".txt")));
		
		JCas ptjcas=xmiFile.getView("plaintext");
		String originalText=ptjcas.getDocumentText();
		
		

		Map<GoldTag,Collection<Sentence>> map_i2b2Annot_sent=JCasUtil.indexCovering(ptjcas, GoldTag.class, Sentence.class);
		Map<Sentence,Collection<WordToken>> map_sent_token=JCasUtil.indexCovered(ptjcas, Sentence.class, WordToken.class);
		Map<Sentence,Collection<MedTimex3>> map_sent_timex=JCasUtil.indexCovered(ptjcas, Sentence.class, MedTimex3.class);
		Map<Sentence,Collection<DrugMentionAnnotation>> map_sent_drug=JCasUtil.indexCovered(ptjcas, Sentence.class, DrugMentionAnnotation.class);
		
		//get the DCT, the first MedTimex3 in the document
		MedTimex3 aDct=null;
		AnnotationIndex<Annotation> timexAnnots=ptjcas.getAnnotationIndex(MedTimex3.type);
        Iterator<Annotation> it_timeAnnots=timexAnnots.iterator();
        while(it_timeAnnots.hasNext()){
        	MedTimex3 amedtime=(MedTimex3)it_timeAnnots.next();
        	aDct=amedtime;
        	break;
        }
		
		AnnotationIndex<Annotation> i2b2Annots=ptjcas.getAnnotationIndex(GoldTag.type);
Iterator<Annotation> it=i2b2Annots.iterator();

//<i2b2Tag, sentence, or neighbor sentences in case the tag spans two sentences>
GoldTag preGoldTag=null;
HashMap<GoldTag,Collection<Sentence>> mapI2b2Sent=new HashMap<GoldTag,Collection<Sentence>>();

StanfordParser sp=new StanfordParser();

		while(it.hasNext()){
			GoldTag currAnno=(GoldTag)it.next();
			if(preGoldTag!=null && preGoldTag.getBegin()==currAnno.getBegin()) continue;
			
			if(!map_i2b2Annot_sent.keySet().contains(currAnno)){
				Collection<Sentence> sent= lookAroundSent(ptjcas,currAnno,1,1);
				
				mapI2b2Sent.put(currAnno, sent);
			}else{
				ArrayList<Sentence> sent=new ArrayList<Sentence>();
				sent.addAll(map_i2b2Annot_sent.get(currAnno));
				if(sent.size()<1){
					sent.addAll(lookAroundSent(ptjcas,currAnno,1,1));
				} 
				else if(sent.get(sent.size()-1).getEnd()<currAnno.getAnnoEnd()){
					sent.addAll(lookAroundSent(ptjcas,currAnno,0,1));
				}
				
				mapI2b2Sent.put(currAnno,sent );
			}

		}
	
		assert i2b2Annots.size()==mapI2b2Sent.keySet().size();
		

		
		for(GoldTag aTag:mapI2b2Sent.keySet()){
			
			ArrayList<WordToken> verbsPOS=new ArrayList<WordToken>();
			String sent_words="";
			ArrayList<MedTimex3> medTimex3s=new ArrayList<MedTimex3>();
			
			Collection<Sentence> sents=mapI2b2Sent.get(aTag);
			if(sents==null){
				System.out.println(" no sents");
				System.exit(0);
			}
		
			// find all verbs in the sentence
			// find all medTimex in the sentence
			
			for(Sentence sent:sents){
				for(WordToken token:map_sent_token.get(sent)){
					String pos=token.getPartOfSpeech();
					if(pos.startsWith("V")||pos.startsWith("M")){
						verbsPOS.add(token);
					}
					sent_words=sent_words+" "+token.getCoveredText();
				}
				
				medTimex3s.addAll(map_sent_timex.get(sent));
			}	
			
			// drugNer dependency parser
			// find all drugMention in the sentence
			// find the new index for aTag
			
			//int aTagBegin=aTag.getBegin();
			int aTagTokenBegin=-1;
			int index=0;
			StringBuilder sb=new StringBuilder();
			for(Sentence sent:sents){
				//init iterators
				Iterator<WordToken> itToken=map_sent_token.get(sent).iterator();
				Iterator<DrugMentionAnnotation> itDrug=null;
				DrugMentionAnnotation drugMention=null;
				if(map_sent_drug.containsKey(sent)){
					itDrug=map_sent_drug.get(sent).iterator();
					drugMention=itDrug.hasNext()? itDrug.next():null;
					setDrugMentionFull(drugMention);
				}
				
				WordToken preToken = null;
				WordToken token = itToken.hasNext()? itToken.next():null;
				boolean isInMed=false;
				while (token!=null || itToken.hasNext()) {
					while(drugMention!=null && isDrugCover(token,drugMention)){
						if(!isInMed){
							sb.append("medication");
							sb.append(" ");
							index++;
							isInMed=true;
						}						
						while(itToken.hasNext() && isDrugCover(token,drugMention)){
							
							if(aTagTokenBegin==-1 && isCoverTag(preToken,token,aTag)){
								aTagTokenBegin=index;
							}
							// if token is under drug, go to the next token
							preToken=token;
							token=itToken.next();
						}
						drugMention=itDrug.hasNext()?itDrug.next():null;
						setDrugMentionFull(drugMention);
					}
					//token is the TagToken?
					if(aTagTokenBegin==-1 && isCoverTag(preToken,token,aTag)){
						aTagTokenBegin=index;
					}
					String tokenStr=token.getCoveredText().toLowerCase();
					if(!tokenStr.trim().equals("")){
						sb.append(tokenStr);
						sb.append(" ");
						index++;
					}
					preToken=token;
					token=itToken.hasNext()? itToken.next():null;
					
				}
				
				//has drug, use normarized sent
				//no drug, use sent_words directly for the dependency parser

				

			}
			
			if(aTagTokenBegin==-1){
				//the tag is in the end of the sentence, the tagName to the end of the sentence
				sb.append(aTag.getTagName());
				aTagTokenBegin=index;
				index++;
			}
			
			int start=aTag.getAnnoStart();
			int end=aTag.getAnnoEnd();
			assert start<=end;
			assert (start>=0)&&(start<originalText.length());
			assert (end>=0)&&(end<originalText.length());

			String indicatorName="";
			if(aTag instanceof GoldMedication){
				GoldMedication medication=(GoldMedication)aTag;	
				indicatorName=medication.getType1();

			}else if(aTag instanceof GoldDisease){
				GoldDisease disease=(GoldDisease)aTag;
				indicatorName=disease.getIndicator();
			}else{
				writer.println(aTag.getClass().getName());
				System.exit(0);
			}
			String strPos="";
			for(WordToken pos:verbsPOS){
				strPos=strPos+"\t"+pos.getPartOfSpeech();
			}
			
			List<String> medTimexLexicons=TimeLexicons.findTimexLexicons(aDct,medTimex3s);
			String strMedTimexLexicons="";
			for(String lexicon:medTimexLexicons){
				strMedTimexLexicons=strMedTimexLexicons+"\t"+lexicon;
			}
			
			//find the nearest timeLexicons to the i2b2Tag
			List<StrIndex> timeLexicons=TimeLexicons.findTimeLexicons(sb.toString());
			String strTimeLexicons="";
			int nearestTimeLexicon=-1;
			int minDiff=Integer.MAX_VALUE;
			for(StrIndex lexicon:timeLexicons){
				strTimeLexicons=strTimeLexicons+"\t"+lexicon.str;
				int curDiff=Math.abs(lexicon.index-aTagTokenBegin);
				if(minDiff>curDiff){
					minDiff=curDiff;
					nearestTimeLexicon=lexicon.index;
				}
			}
			
			String newSent=sb.toString();
			Set<String> relWords=sp.getRelWords(newSent,aTagTokenBegin);
			StringBuilder tempSb=new StringBuilder();
			for(String word:relWords){
				tempSb.append("\t");
				tempSb.append(word);	
			}
			String dependencyRels=tempSb.toString();
			
			// print the dependency parser result
			String dependencyC=sp.printDependCC();
			// print the constituent parser result
			String pennTree=sp.printPenn();
			// print the shortest parth result
			String pathSubtree=null;
			if(nearestTimeLexicon!=-1){
				if(aTagTokenBegin==-1){
					System.out.println(newSent);
					System.out.println(sent_words);
					System.out.println(aTag.getAnnoText());
					System.exit(0);
				}
				pathSubtree=sp.printPenn(sp.pathSubtree(aTagTokenBegin, nearestTimeLexicon));
			}else{
				pathSubtree=pennTree;
			}
			
			//surround words
			ArrayList<WordToken> surroundWords=lookAroundWord(ptjcas,aTag, 5, 5);
			StringBuilder aroundWords=new StringBuilder();
			for(WordToken word:surroundWords){
				aroundWords.append(word.getCoveredText());
				aroundWords.append("\t");
			}
			
			
			writer.println(dependencyC);
			writer.println(pennTree);
			writer.println(pathSubtree);
			writer.println(newSent);
			writer.println(aroundWords.toString());
			writer.println(sent_words);
			writer.println(aTag.getAnnoText());			
			

			//writer.println(aTag.getTime_value()+"\t"+annotText+"\t"+aTag.getSectName()+"\t"+indicatorName+strPos);
			writer.println(aTag.getTime_value()+"\t"+aTag.getSectName()+"\t"+aTag.getTagName()+"\t"
			+indicatorName+strPos+strTimeLexicons+strMedTimexLexicons+dependencyRels);
		}
		//sp.writeWordDict("wordDict.txt");
		writer.close();
	}
	
	private static boolean isCoverTag(Annotation pre, Annotation cur, Annotation target){
		int tagBegin=target.getBegin();
		int tagEnd=target.getEnd();
		if(pre==null){
			//the first token
			if(tagBegin>=cur.getEnd()) return false;
			if(tagEnd<=cur.getBegin()) return true;
			if(tagBegin>=cur.getBegin() && target.getBegin()<cur.getEnd()) return true;
		}else{
			//after cur
			if(tagBegin>=cur.getEnd()) return false;
			//in the front of pre
			if(tagEnd<pre.getBegin()) return false;
			//in cur or in the middle of pre and cur
			if(tagBegin>=pre.getEnd() && tagBegin<cur.getEnd()) return true;
		}
		return false;
	}

	private static void setDrugMentionFull(DrugMentionAnnotation drugMention) {
		// TODO Auto-generated method stub
		if(drugMention==null || drugMention.getBegin()==-1) return;
		if(drugMention.getCoveredText().equals("today")){
			drugMention.setBegin(-1);
			drugMention.setEnd(-1);
			return;
		}
		int endIndex=drugMention.getEnd();
		endIndex=Math.max(endIndex, drugMention.getChangeStatusEnd());
		endIndex=Math.max(endIndex,drugMention.getDosageEnd());
		endIndex=Math.max(endIndex,drugMention.getDurationEnd());
		endIndex=Math.max(endIndex,drugMention.getFormEnd());
		endIndex=Math.max(endIndex,drugMention.getFrequencyEnd());
		endIndex=Math.max(endIndex,drugMention.getFuEnd());
		endIndex=Math.max(endIndex,drugMention.getRouteEnd());
		endIndex=Math.max(endIndex,drugMention.getStrengthEnd());
		endIndex=Math.max(endIndex,drugMention.getSuEnd());
		drugMention.setEnd(endIndex);	
	}

	private static boolean isDrugCover(Annotation token,
			Annotation drugMention) {
		// TODO Auto-generated method stub
		if(token==null || drugMention==null) return false;
		//token in the middle of the drugMention
		if(token.getBegin()>=drugMention.getBegin() && token.getEnd()<=drugMention.getEnd()){
			return true;
		}
		//token at the beginning of the drug Mention
		if(drugMention.getBegin()>=token.getBegin() && drugMention.getBegin()<token.getEnd()){
			return true;
		}
		//token at the end of the drug Mention
		if(drugMention.getEnd()>token.getBegin() && drugMention.getEnd()<=token.getEnd()){
			return true;
		}
		return false;
	}

	private static ArrayList<Sentence> lookAroundSent(JCas ptjcas,GoldTag currAnno, int i, int j) {
		// TODO Auto-generated method stub
		ArrayList<Sentence> sent=new ArrayList<Sentence>();
		List<Sentence> preSent=JCasUtil.selectPreceding(ptjcas, Sentence.class, currAnno,i);
		if(preSent!=null){
			sent.addAll(preSent);
		}
		List<Sentence> aftSent=JCasUtil.selectFollowing(ptjcas, Sentence.class, currAnno, j);
		if(aftSent!=null){
			sent.addAll(aftSent);
		}
		return sent;
	}
	
	private static ArrayList<WordToken> lookAroundWord(JCas ptjcas,GoldTag currAnno, int i, int j) {
		// TODO Auto-generated method stub
		ArrayList<WordToken> rets=new ArrayList<WordToken>();
		List<WordToken> preSent=JCasUtil.selectPreceding(ptjcas, WordToken.class, currAnno,i);
		if(preSent!=null){
			rets.addAll(preSent);
		}
		List<WordToken> aftSent=JCasUtil.selectFollowing(ptjcas, WordToken.class, currAnno, j);
		if(aftSent!=null){
			rets.addAll(aftSent);
		}
		return rets;
	}

	public static void printPOSDS(String inDir,String outDir) throws Exception{
		ArrayList<File> inFiles=listFiles(inDir);
		
		for(File fxmi:inFiles){
			String aXmi=fxmi.getAbsolutePath();
			
			xmi_tag_verb_sec2(aXmi,outDir);

		}
		
	}
	
	public static void main(String[] args) throws Exception {
		
		printPOSDS(args[0],args[1]);
		
	}


}
