package org.apache.ctakes.clinicalpipeline.ae;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JFSIndexRepository;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.cas.TOP;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.ctakes.clinicalpipeline.utils.ProcessCSV;
import org.apache.ctakes.clinicalpipeline.utils.ProcessTimeCSV;
import org.apache.ctakes.clinicalpipeline.utils.ProcessXML;
import org.apache.ctakes.core.util.DocumentIDAnnotationUtil;
import org.apache.ctakes.preprocessor.ClinicalNotePreProcessor;
import org.apache.ctakes.preprocessor.DocumentMetaData;
import org.apache.ctakes.preprocessor.PreProcessor;
import org.apache.ctakes.preprocessor.SegmentMetaData;
import org.apache.ctakes.typesystem.type.i2b2.*;
import org.apache.ctakes.typesystem.type.structured.DocumentID;
import org.apache.ctakes.typesystem.type.textspan.Segment;
import org.apache.ctakes.typesystem.type.util.Pair;
import org.apache.ctakes.typesystem.type.util.Pairs;

public class FeverTagAnnotator extends JCasAnnotator_ImplBase {

	private Logger logger = Logger.getLogger(getClass().getName());
	@Override
	public void process(JCas jcas) throws AnalysisEngineProcessException {
		// TODO Auto-generated method stub
		logger.info("add i2b2 tags");		
    	JCas originalView;
		try {
			originalView = jcas.getView("_InitialView");
			
			String text_csv = originalView.getSofaDataString();

		    
			//create plaintext view, setDcoumentText, add i2b2Annotation
			JCas plaintextView = jcas.createView("plaintext");    
			ProcessCSV processcsv=new ProcessCSV( text_csv,  plaintextView);
			String clinicalNotes=processcsv.getOriginalText();
            plaintextView.setDocumentText(clinicalNotes);
            
            //getMedTime csv
            JCas x= jcas.getView("UriView");
			String originFileName=x.getSofaDataURI();
			String[] values=originFileName.split("\\/");
			String fileName=values[values.length-1];
			String reportName=fileName.split("\\.")[0];
			String filePath="data/medTimeCSV/"+reportName+".csv";
			ProcessTimeCSV ptimeCSV=new ProcessTimeCSV(filePath,plaintextView);
			ptimeCSV.addTimeAnnot();
            
		} catch (CASException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
		
	}
	


}
