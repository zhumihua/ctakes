package org.apache.ctakes.clinicalpipeline.utils;

import org.apache.uima.jcas.JCas;
import org.apache.ctakes.typesystem.type.i2b2.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import au.com.bytecode.opencsv.*;


public class ProcessCSV {
	
	enum I2b2Tag{END,START,DCT,INDICATOR,TAGNAME,ISMED,SECTNAME,TEXT,TIME_AFTER,TIME_BEFORE,TIME_DURING}
	enum SecTag{END,START,TEXT}
	enum I2b2Time{BEFORE, DURING, AFTER}
	//Before During After Not_
	enum TimeValues{NNN,BNN,NDN,BDN,NNA,BNA,NDA,BDA}
	
	public static final String CSV_HEADER="\"a_end\",\"a_start\",\"b_dct\",\"b_diseaseIndic\",\"b_indiName\",\"b_isMed\",\"b_sectName\",\"b_text\",\"time_after\",\"time_before\",\"time_during\"\n";
    public static final String SEG_TAG_HEADER="\"a_end\",\"a_start\",\"b_text\"\n";
	
	private JCas jcas;
	
	private String textCSV;
	private String originalText;
    private String i2b2Tags;
    private String secNameTags;

	
	public ProcessCSV(String xmlText, JCas jcas){
		this.jcas=jcas;
		this.textCSV=xmlText;
		String[] values=this.textCSV.split(CSV_HEADER);
		errorFormat(values,1);
		if(values.length==1){
			this.originalText=values[0];
			
		}else{
			this.originalText=values[0];
			String i2b2_secs=values[1];
			values=i2b2_secs.split(SEG_TAG_HEADER);
			errorFormat(values,0);
			if(values.length<1){
				this.i2b2Tags="";
				this.secNameTags="";
			}else{
				this.i2b2Tags=values[0];
				if(values.length==1){
					this.secNameTags="";
				}else{
					this.secNameTags=values[1];
				}
			}
			this.parseTextCSV();
		}

	}

	private void errorFormat(String[] values,int size) {
		if(values==null||values.length<size){
			System.out.println("wrong format textCSV");
			System.out.println(this.textCSV);
			System.exit(0);
		}
	}

	private void parseTextCSV() {
		// TODO Auto-generated method stub
		CSVReader i2b2TagReader=new CSVReader(new InputStreamReader( new ByteArrayInputStream(this.i2b2Tags.getBytes())));
		
		String[] nextLine;//values from that line
		
		try {
			while((nextLine=i2b2TagReader.readNext())!=null){
				
				int start=Integer.parseInt(nextLine[I2b2Tag.START.ordinal()].trim());
				int end=Integer.parseInt(nextLine[I2b2Tag.END.ordinal()].trim());
				boolean isMed=nextLine[I2b2Tag.ISMED.ordinal()].trim().equals("1")? true:false;
				String indicatorName=nextLine[I2b2Tag.INDICATOR.ordinal()].trim();
				String dct=nextLine[I2b2Tag.DCT.ordinal()].trim();
				String tagName=nextLine[I2b2Tag.TAGNAME.ordinal()].trim();
				String annoText=nextLine[I2b2Tag.TEXT.ordinal()];
				String secName=nextLine[I2b2Tag.SECTNAME.ordinal()].trim();
				int time_before=Integer.parseInt(nextLine[I2b2Tag.TIME_BEFORE.ordinal()].trim());
				int time_during=Integer.parseInt(nextLine[I2b2Tag.TIME_DURING.ordinal()].trim());
				int time_after=Integer.parseInt(nextLine[I2b2Tag.TIME_AFTER.ordinal()].trim());
				int timeValue=calTimeValue(time_before,time_during,time_after);
				
				String trimAnnotText=annoText.trim();
				int tagStart=start+annoText.indexOf(trimAnnotText);
				//int tagEnd=tagStart+1;
				int tagEnd=tagStart+annoText.length();
			
				if(isMed){
					GoldMedication goldMedication=new GoldMedication(jcas);
					goldMedication.setBegin(tagStart);
					goldMedication.setEnd(tagEnd);
					goldMedication.setAnnoStart(start);
					goldMedication.setAnnoEnd(end);
					goldMedication.setIsMed(isMed);
					goldMedication.setType1(indicatorName);
					goldMedication.setDct(dct);
					goldMedication.setTagName(tagName);
					goldMedication.setAnnoText(annoText);
					goldMedication.setTime_before(time_before);
					goldMedication.setTime_during(time_during);
					goldMedication.setTime_after(time_after);
					goldMedication.setTime_value(timeValue);
					goldMedication.setSectName(secName);
					
					
					goldMedication.addToIndexes();
					
				}else{
					GoldDisease goldDisease=new GoldDisease(jcas);
					goldDisease.setBegin(tagStart);
					goldDisease.setEnd(tagEnd);
					goldDisease.setAnnoStart(start);
					goldDisease.setAnnoEnd(end);
					goldDisease.setIsMed(isMed);
					goldDisease.setIndicator(indicatorName);
					goldDisease.setDct(dct);
					goldDisease.setTagName(tagName);
					goldDisease.setAnnoText(annoText);
					goldDisease.setTime_before(time_before);
					goldDisease.setTime_during(time_during);
					goldDisease.setTime_after(time_after);
					goldDisease.setTime_value(timeValue);
					goldDisease.setSectName(secName);
					
					goldDisease.addToIndexes();
				}
					
			}
			i2b2TagReader.close();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(this.secNameTags.equals("")){
			return;
		}
		CSVReader secTagReader=new CSVReader(new InputStreamReader(new ByteArrayInputStream(this.secNameTags.getBytes())));
        
		try {
			while((nextLine=secTagReader.readNext())!=null){
				int start=Integer.parseInt(nextLine[SecTag.START.ordinal()].trim());
				int end=Integer.parseInt(nextLine[SecTag.END.ordinal()].trim());
				String annoText=nextLine[SecTag.TEXT.ordinal()].trim();
				
				Section section=new Section(jcas);
				section.setBegin(start);
				section.setEnd(end);
				section.setSectName(annoText);
				
				section.addToIndexes();
			}
			secTagReader.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private int calTimeValue(int time_before, int time_during, int time_after) {
		// TODO Auto-generated method stub
		return time_before+2*time_during+4*time_after;
	}

	
	public JCas getJcas() {
		return jcas;
	}

	public void setJcas(JCas jcas) {
		this.jcas = jcas;
	}

	public String getXmlText() {
		return textCSV;
	}

	public void setXmlText(String xmlText) {
		this.textCSV = xmlText;
	}

	public void setOriginalText(String originalText) {
		this.originalText = originalText;
	}
	
	public String getOriginalText() {
		return originalText;
	}

}
