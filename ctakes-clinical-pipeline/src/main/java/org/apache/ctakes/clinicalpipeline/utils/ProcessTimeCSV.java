package org.apache.ctakes.clinicalpipeline.utils;

import org.apache.uima.jcas.JCas;
import org.apache.ctakes.medtime.type.MedTimex3;
import org.apache.ctakes.typesystem.type.i2b2.*;

import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import au.com.bytecode.opencsv.*;


public class ProcessTimeCSV {
	
	enum MED_TIME_HEADER{START,END,TIMEXINSANCE,TIMEXTYPE,TIMEXVALUE,TIMEXQUANT,TIMEXFREQ,TIMEXMOD}
	
	
	//public static final String CSV_HEADER="\"a_end\",\"a_start\",\"b_dct\",\"b_diseaseIndic\",\"b_indiName\",\"b_isMed\",\"b_sectName\",\"b_text\",\"time_after\",\"time_before\",\"time_during\"\n";

	
	private JCas jcas;
	private String fileName;
	


	
	public ProcessTimeCSV(String xmlText, JCas jcas){
		this.jcas=jcas;
		this.fileName=xmlText;

	}

	


	public void addTimeAnnot() {
		// TODO Auto-generated method stub
		
		
		String[] nextLine;//values from that line
		int id=0;
		
		try {
			
			CSVReader medTimeCSV=new CSVReader(new FileReader(this.fileName));
			while((nextLine=medTimeCSV.readNext())!=null){
				
				int start=Integer.parseInt(nextLine[MED_TIME_HEADER.START.ordinal()].trim());
				int end=Integer.parseInt(nextLine[MED_TIME_HEADER.END.ordinal()].trim());
				String timetype=nextLine[MED_TIME_HEADER.TIMEXTYPE.ordinal()].trim();
				String timevalue=nextLine[MED_TIME_HEADER.TIMEXVALUE.ordinal()].trim();
				String timeMod=nextLine[MED_TIME_HEADER.TIMEXMOD.ordinal()].trim(); 

				if(timetype.equals("TIME")){
					continue;
				}
				if(id==0&&!timetype.equals("DATE")){
					continue;
				}
				
				
				MedTimex3 medTimeTag=new MedTimex3(jcas);
				medTimeTag.setBegin(start);
				medTimeTag.setEnd(end);
				medTimeTag.setTimexType(timetype);
				medTimeTag.setTimexValue(timevalue);
				medTimeTag.setTimexMod(timeMod);
				medTimeTag.setTimexId(String.valueOf(id));
				id++;
				
				medTimeTag.addToIndexes();
					
			}
			medTimeCSV.close();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}



	
	public JCas getJcas() {
		return jcas;
	}

	public void setJcas(JCas jcas) {
		this.jcas = jcas;
	}




}
