package org.apache.ctakes.clinicalpipeline.utils;

import org.apache.uima.jcas.JCas;
import org.apache.ctakes.typesystem.type.i2b2.*;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;


public class ProcessXML {
 
	private JCas jcas;
	
	private String xmlText;
	private String originalText;
	public String getOriginalText() {
		return originalText;
	}

	public void setOriginalText(String originalText) {
		this.originalText = originalText;
	}

	private ArrayList<Node> medicationNodes;
	private ArrayList<Node> diseaseNodes;
	private Element xmlRoot;
	private Element xmlTags;
	private Document doc;
	
	
	public JCas getJcas() {
		return jcas;
	}

	public void setJcas(JCas jcas) {
		this.jcas = jcas;
	}

	public String getXmlText() {
		return xmlText;
	}

	public void setXmlText(String xmlText) {
		this.xmlText = xmlText;
	}

	public ArrayList<Node> getMedicationNodes() {
		return medicationNodes;
	}

	public void setMedicationNodes(ArrayList<Node> medicationNodes) {
		this.medicationNodes = medicationNodes;
	}

	public ArrayList<Node> getDiseaseNodes() {
		return diseaseNodes;
	}

	public void setDiseaseNodes(ArrayList<Node> diseaseNodes) {
		this.diseaseNodes = diseaseNodes;
	}

	public Element getXmlRoot() {
		return xmlRoot;
	}

	public void setXmlRoot(Element xmlRoot) {
		this.xmlRoot = xmlRoot;
	}

	public Element getXmlTags() {
		return xmlTags;
	}

	public void setXmlTags(Element xmlTags) {
		this.xmlTags = xmlTags;
	}

	public Document getDoc() {
		return doc;
	}

	public void setDoc(Document doc) {
		this.doc = doc;
	}


	public ProcessXML(String xmlText, JCas jcas){
		this.jcas=jcas;
		this.xmlText=xmlText;
		this.diseaseNodes=new ArrayList<Node>();
		this.medicationNodes=new ArrayList<Node>();
		this.parseXML();
	}

	private void parseXML() {
		// TODO Auto-generated method stub
	
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

			this.doc=dBuilder.parse(new InputSource(new ByteArrayInputStream(this.xmlText.getBytes("UTF-8"))));
			this.doc.getDocumentElement().normalize();
			this.xmlRoot=(Element)this.doc.getElementsByTagName("root").item(0);
			this.originalText=parseOriginalText();
			this.xmlTags=(Element)this.xmlRoot.getElementsByTagName("TAGS").item(0);
			this.parseTags();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		
	}

	private String parseOriginalText() {
		
		Node textNode=this.xmlRoot.getElementsByTagName("TEXT").item(0);
		Element element=(Element) textNode;
		Node child=element.getFirstChild();
		if(child instanceof CharacterData){
			CharacterData cd=(CharacterData) child;
			return cd.getData();
		}else{
			return "";
		}

	}

	private void parseTags() {
		// TODO Auto-generated method stub

		try {
			ArrayList<Element> medElements=getDirectChild(xmlTags,"MEDICATION");
			ArrayList<Element> diseaseElements=getDirectChild(xmlTags,"OBESE");
			diseaseElements.addAll(getDirectChild(xmlTags,"OBESE"));
			diseaseElements.addAll(getDirectChild(xmlTags,"DIABETES"));
			diseaseElements.addAll(getDirectChild(xmlTags,"HYPERTENSION"));
			diseaseElements.addAll(getDirectChild(xmlTags,"HYPERLIPIDEMIA"));
			diseaseElements.addAll(getDirectChild(xmlTags,"CAD"));
			
			
			for(Element medelement:medElements){
				NodeList childNodes=medelement.getChildNodes();
				if(childNodes!=null){
					this.medicationNodes.addAll(nodeList2Node(childNodes));
				}
				
			}
			for(Element diseaseelement:diseaseElements){
				NodeList childNodes=diseaseelement.getChildNodes();
				if(childNodes!=null){
					this.diseaseNodes.addAll(nodeList2Node(childNodes));
				}
			}
			this.addJcasIndex();
			

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	 private void addJcasIndex() {
		// TODO Auto-generated method stub
		 for(Node med:this.medicationNodes){
			 Medication medication=new Medication(jcas);
			 
			 medication.setBegin(Integer.parseInt(getValueByAttrName(med,"start")));
			 medication.setEnd(Integer.parseInt(getValueByAttrName(med,"end")));
			 medication.setTime(getValueByAttrName(med,"time"));
			 medication.setType1(getValueByAttrName(med,"type1"));
			 medication.setType2(getValueByAttrName(med,"type2"));
			 medication.setIndicatorName(med.getNodeName());
			 
			 medication.addToIndexes();
		 }
		
		 for(Node disease:this.diseaseNodes){
			 Disease dsa=new Disease(jcas);
			 dsa.setBegin(Integer.parseInt(getValueByAttrName(disease,"start")));
			 dsa.setEnd(Integer.parseInt(getValueByAttrName(disease,"end")));
			 dsa.setIndicator(getValueByAttrName(disease,"indicator"));
			 dsa.setTime(getValueByAttrName(disease,"time"));
			 dsa.setIndicatorName(disease.getNodeName());
			 
			 dsa.addToIndexes();
		 }
	}

	public static ArrayList<Element> getDirectChild(Element parent, String name) {
		 ArrayList<Element> ret=new ArrayList<Element>();
		    for (Node child = parent.getFirstChild(); child != null; child = child.getNextSibling()) {
		      if (child instanceof Element && name.equals(child.getNodeName())) {
		        ret.add((Element) child) ;
		      }
		    }
		    return ret;
		  }
	

	public static ArrayList<Node> nodeList2Node(NodeList nodelist){
		ArrayList<Node> ret=new ArrayList<Node>();
		for(int i=0;i<nodelist.getLength();i++){
			Node node=nodelist.item(i);
			if(node.getNodeType()==Node.ELEMENT_NODE){
				ret.add(node);
			}
			
		}
		return ret;

	}
	
	public static String getValueByAttrName(Node node,String attrName){
		if(node.getNodeType()==Node.ELEMENT_NODE){
			Element element=(Element)node;
			return element.getAttribute(attrName);
		}else{
			System.out.println(node.getNodeName());
			System.out.println(node.getNodeValue());
			System.exit(0);
			return " ";
			}

	}
}
