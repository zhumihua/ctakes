package org.apache.ctakes.clinicalpipeline.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.function.Predicate;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeGraphNode;
import edu.stanford.nlp.trees.TreePrint;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.stanford.nlp.trees.TypedDependency;

public class StanfordParser {
	
	
	  public static void main(String[] args) throws FileNotFoundException {
		  args=new String[]{"csv/test.xls","result.xls"};
	  
		  StanfordParser sp=new StanfordParser();
		  sp.getRelWords("He is very bad", 0);
	  }
	  
	  private  LexicalizedParser lp;
	  private TreebankLanguagePack tlp;
	  private GrammaticalStructureFactory gsf;
	  
	  private Tree parse=null;
	  private TreePrint tpPenn;
	  private TreePrint tpDependC;
	  
	  private Set<String> wordDict;
	  
	  
	  

	  public StanfordParser(){
		  this.lp=LexicalizedParser.loadModel("edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz");
		  this.tlp = new PennTreebankLanguagePack();
		  this.gsf = tlp.grammaticalStructureFactory();
		  
		  this.tpPenn=new TreePrint("penn");
		 // TreePrint tpDepend=new TreePrint("typedDependencies");
		  this.tpDependC=new TreePrint("typedDependenciesCollapsed");
		    
		  this.wordDict=new HashSet<String>();
	  }


	  public Set<String> getRelWords(String sent,int tagIndex){
		  
		  tagIndex++;
		  Set<String> ret=new HashSet<String>();
		  
		  String[] vals=sent.split("\\s");
		  List<CoreLabel> rawWords=Sentence.toCoreLabelList(vals);
		  
		  this.parse = lp.apply(rawWords);
		  GrammaticalStructure gs = gsf.newGrammaticalStructure(parse);
		  List<TypedDependency> dependenciesCC=gs.typedDependenciesCCprocessed();
		  
		for (TypedDependency curRel:dependenciesCC) {
			if (curRel.dep().index() == tagIndex) {
				String strgov=curRel.gov().word();
				ret.add(strgov);
				this.wordDict.add(strgov);
			} else if (curRel.gov().index() == tagIndex) {
				String strdep=curRel.gov().word();
				ret.add(strdep);
				this.wordDict.add(strdep);
			} 
		}
		
		return ret;

	  }
	  
	  public Tree pathSubtree(int indexA, int indexB){
		    List<Tree> leaves=parse.getLeaves();
//		    Tree leaveA=leaves.get(indexA);
//		    Tree leaveB=leaves.get(indexB);
		    final List<Tree> paths=parse.pathNodeToNode(leaves.get(indexA),leaves.get(indexB));
			Predicate<Tree> f=new Predicate<Tree>(){
				@Override
				public boolean test(Tree t) {
					// TODO Auto-generated method stub
					for(Tree tree:paths){
						if(t.contains(tree)){
							return true;
						}
					}
					return false;
				}
		    };
		    return parse.prune(f);
	  }
	  
	  
	  public String printPenn(Tree tree){
		  if(tree==null) return "";
		  StringWriter sw=new StringWriter();
		  PrintWriter pr=new PrintWriter(sw);
		  this.tpPenn.printTree(tree,pr);
		  String parseResult=sw.toString().replace("\n", " ");
		  return parseResult;
	  }
	  
	  public String printDependCC(Tree tree){
		  if(tree==null) return "";
		  StringWriter sw=new StringWriter();
		   PrintWriter pr=new PrintWriter(sw);
		   this.tpDependC.printTree(tree,pr);
		   String parseResult=sw.toString().replace("\n", " ");
		  return parseResult;
	  }
	  
	  public String printPenn(){
		  return this.printPenn(this.parse);
	  }
	  
	  public String printDependCC(){
		  return this.printDependCC(this.parse);
	  }
	  
	  public void writeWordDict(String outputPath){
		  PrintWriter print = null;
		try {
			print = new PrintWriter(new BufferedWriter(new FileWriter(outputPath,true)));
			for(String word:this.wordDict){
				print.println(word);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			print.close();
		}
		
		  
	  }


}
