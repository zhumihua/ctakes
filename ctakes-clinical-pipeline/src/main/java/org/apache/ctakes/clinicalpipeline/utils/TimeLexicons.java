package org.apache.ctakes.clinicalpipeline.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.ctakes.medtime.type.MedTimex3;

import edu.stanford.nlp.time.*;
import edu.stanford.nlp.util.Pair;

public class TimeLexicons {

	public static final String beforeSeq = "afore | aforesaid | aforetime | ahead | ahead of | already | and then | ante | ante- |  antecedent | antecedently | before | before present | beforehand | by the time | earlier | early | ere | first | firstly | fore | foregoing | former | formerly | heretofore | in advance | in advance of | in days of yore | in old days | on the eve of | once | past | pre- | precede | preceded | precedent | precedes | preceding | preceding the time when | precipitate |  precipitated | precipitates | precipitating | precursive | precursory | preliminary | preop | preoperatively | preparatory | preprandial | previous | previous to | previously | prior |prior to | since | sooner | sooner than | then | to come | until | up front | up to now ";

	public static final String afterSeq = "a while later | after | after that | after this | afterward | afterwards | at a later time | behind | by and by | consequently | ensuingly | eventually | follow | followed | following | follows | from that day forward | from that day on | from there forward | from there on | hereafter | in a while | later | latterly | next | on the next day | post | post- | postoperative | postoperatively | since | soon | subsequent | subsequently | then | thenceforth | thenceforward | thereafter | thereon | ultimately ";

	public static final String causesSeq = "accordingly | account for | accounted for | accounting for | accounts for | affect | affected | affecting | affects | are responsible for | arouse | aroused | arouses | arousing | as a consequence | as a result | as expected | became | because | because of | become | becomes | becoming | block | blocked | blocking | blocks | bring about | brings about | brought about | can generate | can lead to | cause | cause of | cause these symptoms | caused | caused by | causes | causing | change | changed | changes | changing | consequently | contribute to | contributed to | contributes to | contributing to | could generate | create | created | creates | creating | develop | developed | developing | develops | due to | effect | enable | enabled | enables | enabling | evoke | evoked | evokes | evoking | for | for this reason | force | forced forces | forcing | form | formed | forming | forms | gave rise to | generate | generated | generates | generating | give rise to | given | gives rise to | giving rise to | had an effect | hamper | hampered | hampering | hampers | has an effect | have an effect | having an effect | impede | impeded | impedes | impeding | in consequence | in effect | incite | incited | incites | inciting | increase | increased | increases | increasing | indicate | indicated | indicates | indicating | induce | induced | induces | inducing | influence | influenced | influences | influencing | interfere with | interfered with | interferes with | interfering with | is blamed for | is responsible for | lead to | leading to | leads to | leave | leaves | leaving | led to | left | made | made possible | make | make possible | makes | makes possible | making | making possible | motivate | motivated | motivates | motivating | perpetuate | perpetuated |perpetuates | perpetuating | play a role in | played a role in | playing a role in | plays a role in | precipitate | precipitated | precipitates | precipitating | prerequisite | prerequisites | prevent | prevented | preventing | prevents | produce | produced | produces | producing | promote | promoted | promotes | promoting | prompt | prompted | prompting | prompts | provoke | provoked | provokes | provoking | raise | raised | raises | raising | reduce | reduced | reduces | reducing | render | rendered | rendering | renders | require | required | requires | requiring | restrain | restrained | restraining | restrains | result | result in | resulted | resulted in | resulting | resulting in | results | results in | since | slow | slowed | stimulates | stimulating | such that | the reason for | trigger | triggered | triggering | triggers | was responsible for | yield | yielded | yielding | yields";

	public static final String causedBySeq = "as soon as | attributed to | because | because of | derive from | derived from | deriving from | due to | effect of | follow from | followed from | following from | follows from | is attributed to | is a result of | is a consequence of | now that | occur from | occurred from | occurring from | occurs from | on account of | originate from | originate in | originate with | originated from | originated in | originated with | originates from | originates in | originates with | originating from | originating in | originating with | owing to | result from | result of | resulted from | resulting from | results from | stem from | stemmed from | stemming from | stems from | thanks to";

	public static final String duringSeq = "accompanied | accompanies | accompanying | ad interim | all along | all the time | all the while | all through | amid | at that same time | at that time | at the same time | at the time | at this same time | at this time as well | attendant | coexist | coexisted | coexisting | coincide | coincided | coincident | coincidentally | coincides | coinciding | coinciding | concomitant | concomitantly | concurrent | concurrently | conjoint | conjointly | contemporaneous | contemporaneously | during | during the interval | for now | for the duration | for the moment | for the time being | from beginning to end | from start to finish | in conjunction | in the course of | in the interim | in the interval | in the meantime | in the meanwhile | in the middle of | in the time of | inextricably | inseparably | intraoperatively | jointly | linked | meantime | meanwhile | mid | midst | over | pending | simultaneous | simultaneously | synchronically | synchronous | synchronously | the time between | the whole time | through the whole of | throughout | throughout the | together | when | when the patient | while";

	public static final String startingSeq = "at first | at the start | began | beginning | begins | begun | commence | commenced | commences | commencing | embark on | embarked on | embarking on | embarks on | first of all | get started | gets started | getting started | got started | in the beginning | initial | initially | initiate | initiated | initiates | initiating | lead off | leading off | leads off | led off | onset | recommence | recommenced | recommences | recommencing | restart | restarted | restarting | restarts | set out | sets out | setting out | start | start out | started | started out | starting | starting out | starts | starts out";

	public static final String continuingSeq = "continually | continue | continues | continuing | continuous | lasting | lasts | ongoing | persisting | persists | prolonged | recurrence | recurring | remain | remained | remaining | resumed | still | sustained | sustaining | unchanged | unchanging | undergoing";

	public static final String endingSeq = "after all | at last | at length | at long last | at the close | at the end of the day | conclusively | eventually | finally | in due time | in the end | in the long run | lastly | someday | ultimately";

	public static final String suddenlySeq = "abruptly | all at once | all of a sudden | asudden | immediately | instantly | just then | on spur of moment | precipitately | straightaway | sudden | suddenly | tout de suite | unexpectedly | without warning";

	public static final String nowSeq = "at this point | currently | now | right away | right now | soon ";

	public static final String saysSeq = "says | said | note | notes | noted | told | tells | presents | presented | describes | described | states | stated | denied | denies | complains | complained | report | reports | reported ";
	
	public static final String timexBefore="BEFORE|ON_OR_BEFORE";
	public static final String timexAfter="AFTER|ON_OR_AFTER";
	public static final String timexDuring="DURING|LESS_THAN|MORE_THAN|EQUAL_OR_LESS|EQUAL_OR_MORE";
	
	public static final String timexStart="START";
	public static final String timexMid="MID";
	public static final String timexEnd="END";
	
	
	public static final String DATE="DATE";
	public static final String SET="SET";
	public static final String DURATION="DURATION";
	
	
	
	public static void main(String[] args){
		String strs="He is no longer taking the hydrochlorothiazide that was prescribed to him in the past";
		getStrIndex( strs,"past","before");
		
	}
	
	public static StrIndex getStrIndex(String strs,String substr,String targetStr){
		int index=strs.indexOf(substr);
		int ret=0;
		for(int i=0;i<index;i++){
			if(strs.charAt(i)==' ' && i>0 && strs.charAt(i-1)!=' '){
				ret++;
			//	System.out.println(strs.charAt(i)+" "+ret);
			}
		}
		return new StrIndex(targetStr,ret);
	}
	public static List<StrIndex> findTimeLexicons(String sent_words) {
		// TODO Auto-generated method stub
		sent_words=sent_words.toLowerCase().trim();
		//String[] vals=sent_words.split("[\\s]+");
		List<StrIndex> ret=new ArrayList<StrIndex>();
		//before,after,cause,causedBy,during,starting,continuing,ending,suddenly,now,says'
		String[] befores=TimeLexicons.beforeSeq.split("\\|");
		for(String before:befores){
			if(sent_words.contains(before.trim())){
				sent_words.indexOf(before.trim());
				ret.add(getStrIndex(sent_words,before.trim(),"before"));
				break;
			}
		}
		String [] afters=TimeLexicons.afterSeq.split("\\|");
		for(String after:afters){
			if(sent_words.contains(after.trim())){
				ret.add(getStrIndex(sent_words,after.trim(),"after"));
				break;
			}
		}
		
		String [] causes=TimeLexicons.causesSeq.split("\\|");
		for(String cause:causes){
			if(sent_words.contains(cause.trim())){
				ret.add(getStrIndex(sent_words,cause.trim(),"cause"));
				break;
			}
		}
		
		String [] causeBys=TimeLexicons.causedBySeq.split("\\|");
		for(String causeby:causeBys){
			if(sent_words.contains(causeby.trim())){
				ret.add(getStrIndex(sent_words,causeby.trim(),"causedBy"));
				break;
			}
		}
		
		String [] durings=TimeLexicons.duringSeq.split("\\|");
		for(String during:durings){
			if(sent_words.contains(during.trim())){
				ret.add(getStrIndex(sent_words,during.trim(),"during"));
				break;
			}
		}
		
		String [] startings=TimeLexicons.startingSeq.split("\\|");
		for(String starting:startings){
			if(sent_words.contains(starting.trim())){
				//ret.add("starting");
				ret.add(getStrIndex(sent_words,starting.trim(),"starting"));
				break;
			}
		}
		
		String [] continuings=TimeLexicons.continuingSeq.split("\\|");
		for(String continuing:continuings){
			if(sent_words.contains(continuing.trim())){
				//ret.add("continuing");
				ret.add(getStrIndex(sent_words,continuing.trim(),"continuing"));
				break;
			}
		}
		
		String [] endings=TimeLexicons.endingSeq.split("\\|");
		for(String ending:endings){
			if(sent_words.contains(ending.trim())){
				//ret.add("ending");
				ret.add(getStrIndex(sent_words,ending.trim(),"ending"));
				break;
			}
		}
		
		String [] suddenlys=TimeLexicons.suddenlySeq.split("\\|");
		for(String suddenly:suddenlys){
			if(sent_words.contains(suddenly.trim())){
				//ret.add("suddenly");
				ret.add(getStrIndex(sent_words,suddenly.trim(),"suddenly"));
				break;
			}
		}
		
		String [] nows=TimeLexicons.nowSeq.split("\\|");
		for(String now:nows){
			if(sent_words.contains(now.trim())){
				ret.add(getStrIndex(sent_words,now.trim(),"now"));
				break;
			}
		}
		
		String [] sayss=TimeLexicons.saysSeq.split("\\|");
		for(String says:sayss){
			if(sent_words.contains(says.trim())){
				ret.add(getStrIndex(sent_words,says.trim(),"says"));
				break;
			}
		}
		
		return ret;
	}


/**
 * DCT_BEFORE,DCT_AFTER,DCT_DURING,TIMEX_END,TIMEX_MID,TIMEX_START,TIMEX_DURING,TIMEX_BEFORE,TIMEX_DURING,TYPE_DURING,TYPE_SET
 * @param aDct
 * @param medTimex3s
 * @return
 */

	public static List<String> findTimexLexicons(
			MedTimex3 aDct, ArrayList<MedTimex3> medTimex3s) {
		// TODO Auto-generated method stub
		List<String> ret=new ArrayList<String>();
		if(aDct==null||medTimex3s.size()<1){
			return ret;
		}
		Timex theDCT=new Timex(aDct.getTimexType(),aDct.getTimexValue());
		Calendar dctDate=theDCT.getDate();
		
		for(MedTimex3 medTimex:medTimex3s){
			Timex aTimex=new Timex(medTimex.getTimexType(),medTimex.getTimexValue());
			String typeName = medTimex.getTimexType().toUpperCase();
			if(typeName.equals(TimeLexicons.DATE)){
				try{
					Calendar aDate=aTimex.getDate();
					if(aDate.after(dctDate)){
						ret.add("DCT_AFTER");
					}else if(aDate.before(dctDate)){
						ret.add("DCT_BEFORE");
					}else if(aDate.equals(dctDate)){
						ret.add("DCT_DURING");
					}
				}catch(Exception e){
					try{
						Pair<Calendar,Calendar> aPair=aTimex.getRange();
						Calendar first=aPair.first();
						Calendar second=aPair.second();
						if(dctDate.after(second)){
							ret.add("DCT_BEFORE");
						}
						if(dctDate.before(first)){
							ret.add("DCT_AFTER");
						}
						if(dctDate.after(first)&&dctDate.before(second)){
							ret.add("DCT_DURING");
						}
						
					}catch(Exception e2){
						
					}
					
				}
				
				
			}
			if(typeName.equals(TimeLexicons.DURATION)){
				ret.add("TYPE_DURING");
			}
			if(typeName.equals(TimeLexicons.SET)){
				ret.add("TYPE_SET");
			}
			
			
			String modName = medTimex.getTimexMod().toUpperCase();
			if(modName.equals(timexEnd)){
				ret.add("TIMEX_END");
			}
			if(modName.equals(timexMid)){
				ret.add("TIMEX_MID");
			}
			if(modName.equals(timexStart)){
				ret.add("TIMEX_START");
			}
			
			if(TimeLexicons.timexAfter.contains(modName)){
				ret.add("TIMEX_AFTER");
			}
			if(TimeLexicons.timexBefore.contains(modName)){
				ret.add("TIMEX_BEFORE");
			}
			if(TimeLexicons.timexDuring.contains(modName)){
				ret.add("TIMEX_DURING");
			}


		}
		
		return ret;
	}

}
