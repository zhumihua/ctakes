

/* First created by JCasGen Tue Dec 09 23:28:15 EST 2014 */
package org.apache.ctakes.constituency.parser.uima.type;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Tue Dec 09 23:28:15 EST 2014
 * XML source: /Users/lusisi/i2b2/apache-ctakes-3.1.1-src/ctakes-constituency-parser/src/main/resources/org/apache/ctakes/constituency/parser/types/LabeledTree.xml
 * @generated */
public class BooleanLabeledTree extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(BooleanLabeledTree.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected BooleanLabeledTree() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public BooleanLabeledTree(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public BooleanLabeledTree(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public BooleanLabeledTree(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: label

  /** getter for label - gets 
   * @generated */
  public boolean getLabel() {
    if (BooleanLabeledTree_Type.featOkTst && ((BooleanLabeledTree_Type)jcasType).casFeat_label == null)
      jcasType.jcas.throwFeatMissing("label", "org.apache.ctakes.constituency.parser.uima.type.BooleanLabeledTree");
    return jcasType.ll_cas.ll_getBooleanValue(addr, ((BooleanLabeledTree_Type)jcasType).casFeatCode_label);}
    
  /** setter for label - sets  
   * @generated */
  public void setLabel(boolean v) {
    if (BooleanLabeledTree_Type.featOkTst && ((BooleanLabeledTree_Type)jcasType).casFeat_label == null)
      jcasType.jcas.throwFeatMissing("label", "org.apache.ctakes.constituency.parser.uima.type.BooleanLabeledTree");
    jcasType.ll_cas.ll_setBooleanValue(addr, ((BooleanLabeledTree_Type)jcasType).casFeatCode_label, v);}    
   
    
  //*--------------*
  //* Feature: tree

  /** getter for tree - gets 
   * @generated */
  public String getTree() {
    if (BooleanLabeledTree_Type.featOkTst && ((BooleanLabeledTree_Type)jcasType).casFeat_tree == null)
      jcasType.jcas.throwFeatMissing("tree", "org.apache.ctakes.constituency.parser.uima.type.BooleanLabeledTree");
    return jcasType.ll_cas.ll_getStringValue(addr, ((BooleanLabeledTree_Type)jcasType).casFeatCode_tree);}
    
  /** setter for tree - sets  
   * @generated */
  public void setTree(String v) {
    if (BooleanLabeledTree_Type.featOkTst && ((BooleanLabeledTree_Type)jcasType).casFeat_tree == null)
      jcasType.jcas.throwFeatMissing("tree", "org.apache.ctakes.constituency.parser.uima.type.BooleanLabeledTree");
    jcasType.ll_cas.ll_setStringValue(addr, ((BooleanLabeledTree_Type)jcasType).casFeatCode_tree, v);}    
  }

    