
/* First created by JCasGen Tue Dec 09 23:28:15 EST 2014 */
package org.apache.ctakes.constituency.parser.uima.type;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** 
 * Updated by JCasGen Tue Dec 09 23:28:15 EST 2014
 * @generated */
public class BooleanLabeledTree_Type extends Annotation_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (BooleanLabeledTree_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = BooleanLabeledTree_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new BooleanLabeledTree(addr, BooleanLabeledTree_Type.this);
  			   BooleanLabeledTree_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new BooleanLabeledTree(addr, BooleanLabeledTree_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = BooleanLabeledTree.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.apache.ctakes.constituency.parser.uima.type.BooleanLabeledTree");
 
  /** @generated */
  final Feature casFeat_label;
  /** @generated */
  final int     casFeatCode_label;
  /** @generated */ 
  public boolean getLabel(int addr) {
        if (featOkTst && casFeat_label == null)
      jcas.throwFeatMissing("label", "org.apache.ctakes.constituency.parser.uima.type.BooleanLabeledTree");
    return ll_cas.ll_getBooleanValue(addr, casFeatCode_label);
  }
  /** @generated */    
  public void setLabel(int addr, boolean v) {
        if (featOkTst && casFeat_label == null)
      jcas.throwFeatMissing("label", "org.apache.ctakes.constituency.parser.uima.type.BooleanLabeledTree");
    ll_cas.ll_setBooleanValue(addr, casFeatCode_label, v);}
    
  
 
  /** @generated */
  final Feature casFeat_tree;
  /** @generated */
  final int     casFeatCode_tree;
  /** @generated */ 
  public String getTree(int addr) {
        if (featOkTst && casFeat_tree == null)
      jcas.throwFeatMissing("tree", "org.apache.ctakes.constituency.parser.uima.type.BooleanLabeledTree");
    return ll_cas.ll_getStringValue(addr, casFeatCode_tree);
  }
  /** @generated */    
  public void setTree(int addr, String v) {
        if (featOkTst && casFeat_tree == null)
      jcas.throwFeatMissing("tree", "org.apache.ctakes.constituency.parser.uima.type.BooleanLabeledTree");
    ll_cas.ll_setStringValue(addr, casFeatCode_tree, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public BooleanLabeledTree_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_label = jcas.getRequiredFeatureDE(casType, "label", "uima.cas.Boolean", featOkTst);
    casFeatCode_label  = (null == casFeat_label) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_label).getCode();

 
    casFeat_tree = jcas.getRequiredFeatureDE(casType, "tree", "uima.cas.String", featOkTst);
    casFeatCode_tree  = (null == casFeat_tree) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_tree).getCode();

  }
}



    