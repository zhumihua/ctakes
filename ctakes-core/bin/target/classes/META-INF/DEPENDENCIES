// ------------------------------------------------------------------
// Transitive dependencies of this project determined from the
// maven pom organized by organization.
// ------------------------------------------------------------------

Apache cTAKES core


From: 'an unknown organization'
  - AOP alliance (http://aopalliance.sourceforge.net) aopalliance:aopalliance:jar:1.0
    License: Public Domain 
  - FindBugs-jsr305 (http://findbugs.sourceforge.net/) com.google.code.findbugs:jsr305:jar:1.3.9
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - Guava: Google Core Libraries for Java (http://code.google.com/p/guava-libraries/guava) com.google.guava:guava:jar:10.0
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - sqlwrapper (http://code.google.com/p/armbrust-file-utils/) com.googlecode.armbrust-file-utils:sqlwrapper:jar:0.0.1
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - FindStructAPI (http://groups.csail.mit.edu/medg/projects/text/findstruct) edu.mit.findstruct:findstructapi:jar:0.0.1
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - jakarta-regexp  jakarta-regexp:jakarta-regexp:jar:1.4

  - jdom  jdom:jdom:jar:1.0

  - jwnl  jwnl:jwnl:jar:1.3.3

  - OpenAI_FSM (https://sourceforge.net/projects/openai/) net.sourceforge.openai:openaifsm:jar:0.0.1
    License: BSD License  (http://opensource.org/licenses/BSD-2-Clause)
  - spring-aop  org.springframework:spring-aop:jar:3.1.0.RELEASE

  - spring-asm  org.springframework:spring-asm:jar:3.1.2.RELEASE
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - spring-beans  org.springframework:spring-beans:jar:3.1.0.RELEASE

  - spring-context  org.springframework:spring-context:jar:3.1.0.RELEASE

  - spring-core  org.springframework:spring-core:jar:3.1.2.RELEASE
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - spring-expression  org.springframework:spring-expression:jar:3.1.0.RELEASE

  - uimaFIT (http://uimafit.googlecode.com) org.uimafit:uimafit:jar:1.4.0
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - LIBSVM (http://www.csie.ntu.edu.tw/~cjlin/libsvm/) tw.edu.ntu.csie:libsvm:jar:3.1
    License: BSD 3-Clause License  (http://www.csie.ntu.edu.tw/~cjlin/libsvm/COPYRIGHT)

From: 'Apache Software Foundation' (http://www.apache.org)
  - Apache Log4j (http://logging.apache.org/log4j/1.2/) log4j:log4j:bundle:1.2.16
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)

From: 'JDOM' (http://www.jdom.org)
  - JDOM (http://www.jdom.org) org.jdom:jdom2:jar:2.0.3
    License: Similar to Apache License but with the acknowledgment clause removed  (https://raw.github.com/hunterhacker/jdom/master/LICENSE.txt)

From: 'The Apache Software Foundation' (http://www.apache.org/)
  - Commons IO (http://commons.apache.org/io/) commons-io:commons-io:jar:2.0.1
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - Commons Lang (http://commons.apache.org/lang/) commons-lang:commons-lang:jar:2.4
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - Commons Logging (http://commons.apache.org/logging) commons-logging:commons-logging:jar:1.1.1
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - Apache cTAKES Resources core (http://ctakes.apache.org/ctakes-core-res) org.apache.ctakes:ctakes-core-res:jar:3.1.1
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - Apache cTAKES common type system (http://ctakes.apache.org/ctakes-type-system) org.apache.ctakes:ctakes-type-system:jar:3.1.1
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - Apache cTAKES utils (http://ctakes.apache.org/ctakes-utils) org.apache.ctakes:ctakes-utils:jar:3.1.1
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - Lucene Common Analyzers (http://lucene.apache.org/lucene-parent/lucene-analyzers-common) org.apache.lucene:lucene-analyzers-common:jar:4.0.0
    License: Apache 2  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - Lucene Core (http://lucene.apache.org/lucene-parent/lucene-core) org.apache.lucene:lucene-core:jar:4.0.0
    License: Apache 2  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - Lucene Queries (http://lucene.apache.org/lucene-parent/lucene-queries) org.apache.lucene:lucene-queries:jar:4.0.0
    License: Apache 2  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - Lucene QueryParsers (http://lucene.apache.org/lucene-parent/lucene-queryparser) org.apache.lucene:lucene-queryparser:jar:4.0.0
    License: Apache 2  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - Lucene Sandbox (http://lucene.apache.org/lucene-parent/lucene-sandbox) org.apache.lucene:lucene-sandbox:jar:4.0.0
    License: Apache 2  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - OpenNLP Maxent (http://www.apache.org/opennlp-maxent/) org.apache.opennlp:opennlp-maxent:bundle:3.0.2-incubating
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - OpenNLP Tools (http://www.apache.org/opennlp-tools/) org.apache.opennlp:opennlp-tools:bundle:1.5.2-incubating
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - Apache UIMA Base: jVinci: Vinci Transport Library (http://uima.apache.org) org.apache.uima:jVinci:jar:2.4.0
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - Apache UIMA Base: uimaj-adapter-vinci: Vinci Adapter (http://uima.apache.org) org.apache.uima:uimaj-adapter-vinci:jar:2.4.0
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - UIMA Base: uimaj-core (http://uima.apache.org) org.apache.uima:uimaj-core:jar:2.4.0
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - Apache UIMA Base: uimaj-cpe: Collection Processing Engine (http://uima.apache.org) org.apache.uima:uimaj-cpe:jar:2.4.0
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - Apache UIMA Base: uimaj-document-annotation (http://uima.apache.org) org.apache.uima:uimaj-document-annotation:jar:2.4.0
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - Apache UIMA Base: uimaj-examples: SDK Examples (http://uima.apache.org) org.apache.uima:uimaj-examples:jar:2.4.0
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)
  - Apache UIMA Base: uimaj-tools: Tools (http://uima.apache.org) org.apache.uima:uimaj-tools:jar:2.4.0
    License: The Apache Software License, Version 2.0  (http://www.apache.org/licenses/LICENSE-2.0.txt)




