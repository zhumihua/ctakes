#
DIRBASE=/home/sisi/i2b2P/python
DIRTEXT=${DIRBASE}/kd_data_text/
DIRXMI=${DIRBASE}/kd_sent_xmi/
DIRCSV=${DIRBASE}/kd_sent_csv/

rm -rf ${DIRXMI} ${DIRCSV}
mkdir ${DIRXMI} ${DIRCSV}

export MAVEN_OPTS="-Xmx2048m"
mvn -Dmaven.test.skip=true clean compile -PrunSentSplit -DinputText=${DIRTEXT} -DoutputXMI=${DIRXMI}

mvn -Dmaven.test.skip=true clean compile -PrunSentCSV -DinputXMI=${DIRXMI} -DoutputCSV=${DIRCSV}

