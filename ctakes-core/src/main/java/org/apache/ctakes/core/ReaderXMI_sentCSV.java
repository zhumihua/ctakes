package org.apache.ctakes.core;


import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import org.apache.ctakes.core.util.CtakesFileNamer;
import org.apache.ctakes.typesystem.type.textspan.Sentence;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.uimafit.component.xwriter.XWriter;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.pipeline.SimplePipeline;
import org.uimafit.factory.JCasFactory;

import au.com.bytecode.opencsv.CSVWriter;

/**
 * Run the plaintext clinical pipeline, using the dictionary of terms from UMLS.
 * Note you must have the UMLS password supplied in some way - see the 
 * User or Developer Guide for information on options for doing that.
 * Also note you need to have the UMLS dictionaries available (they 
 * are separate download from Apache cTAKES itself due to licensing).
 * 
 * Input and output directory names are taken from {@link AssertionConst} 
 * 
 */
public class ReaderXMI_sentCSV {


	private JCas xmiFile;
	
	
	
	
	public static void printSent(String sent){
		System.out.println("`````````````");
		System.out.println(sent);
		System.out.println("`````````````");
	}

	
	public static ArrayList<File> listFiles(String dirName){
		File folder=new File(dirName);
		File[] listFiles=folder.listFiles();
		
		ArrayList<File> rets=new ArrayList<File>();
		for(int i=0;i<listFiles.length;i++){
			if(listFiles[i].isFile()&&listFiles[i].getName().endsWith(".xmi")){
				rets.add(listFiles[i]);
			}	
			}
		return rets;
	}
	
	public static void printSentCSV(String inDir,String out_csvDir) throws Exception{
		ArrayList<File> inFiles=listFiles(inDir);
		
		for(File fxmi:inFiles){
			String in_xmiFName=fxmi.getAbsolutePath();
			
			xmi_sentCSV(in_xmiFName,out_csvDir);

		}
		
	}

	public static void xmi_sentCSV(String in_xmiFName,String out_csvDir) throws Exception{
		
		JCas xmiFile=JCasFactory.createJCas();
		JCasFactory.loadJCas(xmiFile, in_xmiFName);
		JCas x= xmiFile.getView("UriView");
		String originFileName=x.getSofaDataURI();
		String[] values=originFileName.split("\\/");
		String fileName=values[values.length-1];
		fileName=fileName.substring(0,fileName.lastIndexOf("."));
		CSVWriter writer = new CSVWriter(new FileWriter(out_csvDir+fileName+".csv"));
		

		AnnotationIndex<Annotation> sentAnnot=xmiFile.getAnnotationIndex(Sentence.type);
		Iterator<Annotation> it=sentAnnot.iterator();
		
		while(it.hasNext()){
			Annotation currAnno=it.next();
			int start=currAnno.getBegin();
			int end=currAnno.getEnd();
			String sent=currAnno.getCoveredText();
			//printSent(sent);
			String[] entires = new String[] { String.valueOf(start),
					String.valueOf(end),sent};
			writer.writeNext(entires);
		}
		

		writer.close();
		
	}
	
	public static void main(String[] args) throws Exception {

		if(args==null||args.length<2){System.out.println("no args"); System.exit(0);}
		
		printSentCSV(args[0],args[1]);

	}


}
