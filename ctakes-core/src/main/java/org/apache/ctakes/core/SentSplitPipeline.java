

package org.apache.ctakes.core;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import org.apache.ctakes.core.util.CtakesFileNamer;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.collection.CollectionReader;
import org.uimafit.component.xwriter.XWriter;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.pipeline.SimplePipeline;
import org.cleartk.util.cr.FilesCollectionReader;

/**
 * Run the plaintext clinical pipeline, using the dictionary of terms from UMLS.
 * Note you must have the UMLS password supplied in some way - see the 
 * User or Developer Guide for information on options for doing that.
 * Also note you need to have the UMLS dictionaries available (they 
 * are separate download from Apache cTAKES itself due to licensing).
 * 
 * Input and output directory names are taken from {@link AssertionConst} 
 * 
 */
public class SentSplitPipeline {

	public File inputDirectory;
	

	
	public static void main(String[] args) throws Exception {

	    System.out.println("Started " + SentSplitPipeline.class.getCanonicalName() + " at " + new Date());


	    CollectionReader collectionReader = FilesCollectionReader.getCollectionReader(args[0]);
	    
		

		AnalysisEngineDescription pipelineIncludingUmlsDictionaries = AnalysisEngineFactory.createAnalysisEngineDescription(
				"desc/analysis_engine/SentenceDetectorAggregate");
		
		AnalysisEngineDescription xWriter = AnalysisEngineFactory.createPrimitiveDescription(
		XWriter.class,
		XWriter.PARAM_OUTPUT_DIRECTORY_NAME,
		args[1],
		//"data/output",
		//AssertionConst.evalOutputDir,
        XWriter.PARAM_XML_SCHEME_NAME,
        XWriter.XMI);

		SimplePipeline.runPipeline(collectionReader, pipelineIncludingUmlsDictionaries, xWriter);
		
	    System.out.println("Done at " + new Date());
	}


}
