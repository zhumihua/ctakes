

/* First created by JCasGen Tue Dec 09 23:28:43 EST 2014 */
package org.apache.ctakes.coreference.type;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.cas.TOP;


/** 
 * Updated by JCasGen Tue Dec 09 23:28:43 EST 2014
 * XML source: /Users/lusisi/i2b2/apache-ctakes-3.1.1-src/ctakes-coreference/src/main/resources/org/apache/ctakes/coreference/types/TypeSystem.xml
 * @generated */
public class BooleanLabeledFS extends TOP {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(BooleanLabeledFS.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected BooleanLabeledFS() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public BooleanLabeledFS(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public BooleanLabeledFS(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: label

  /** getter for label - gets 
   * @generated */
  public boolean getLabel() {
    if (BooleanLabeledFS_Type.featOkTst && ((BooleanLabeledFS_Type)jcasType).casFeat_label == null)
      jcasType.jcas.throwFeatMissing("label", "org.apache.ctakes.coreference.type.BooleanLabeledFS");
    return jcasType.ll_cas.ll_getBooleanValue(addr, ((BooleanLabeledFS_Type)jcasType).casFeatCode_label);}
    
  /** setter for label - sets  
   * @generated */
  public void setLabel(boolean v) {
    if (BooleanLabeledFS_Type.featOkTst && ((BooleanLabeledFS_Type)jcasType).casFeat_label == null)
      jcasType.jcas.throwFeatMissing("label", "org.apache.ctakes.coreference.type.BooleanLabeledFS");
    jcasType.ll_cas.ll_setBooleanValue(addr, ((BooleanLabeledFS_Type)jcasType).casFeatCode_label, v);}    
   
    
  //*--------------*
  //* Feature: feature

  /** getter for feature - gets 
   * @generated */
  public TOP getFeature() {
    if (BooleanLabeledFS_Type.featOkTst && ((BooleanLabeledFS_Type)jcasType).casFeat_feature == null)
      jcasType.jcas.throwFeatMissing("feature", "org.apache.ctakes.coreference.type.BooleanLabeledFS");
    return (TOP)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((BooleanLabeledFS_Type)jcasType).casFeatCode_feature)));}
    
  /** setter for feature - sets  
   * @generated */
  public void setFeature(TOP v) {
    if (BooleanLabeledFS_Type.featOkTst && ((BooleanLabeledFS_Type)jcasType).casFeat_feature == null)
      jcasType.jcas.throwFeatMissing("feature", "org.apache.ctakes.coreference.type.BooleanLabeledFS");
    jcasType.ll_cas.ll_setRefValue(addr, ((BooleanLabeledFS_Type)jcasType).casFeatCode_feature, jcasType.ll_cas.ll_getFSRef(v));}    
  }

    