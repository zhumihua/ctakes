

/* First created by JCasGen Tue Dec 09 23:28:43 EST 2014 */
package org.apache.ctakes.coreference.type;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.cas.TOP;


/** 
 * Updated by JCasGen Tue Dec 09 23:28:43 EST 2014
 * XML source: /Users/lusisi/i2b2/apache-ctakes-3.1.1-src/ctakes-coreference/src/main/resources/org/apache/ctakes/coreference/types/TypeSystem.xml
 * @generated */
public class MarkablePair extends TOP {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(MarkablePair.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected MarkablePair() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public MarkablePair(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public MarkablePair(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: antecedent

  /** getter for antecedent - gets Proposed antecedent
   * @generated */
  public Markable getAntecedent() {
    if (MarkablePair_Type.featOkTst && ((MarkablePair_Type)jcasType).casFeat_antecedent == null)
      jcasType.jcas.throwFeatMissing("antecedent", "org.apache.ctakes.coreference.type.MarkablePair");
    return (Markable)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((MarkablePair_Type)jcasType).casFeatCode_antecedent)));}
    
  /** setter for antecedent - sets Proposed antecedent 
   * @generated */
  public void setAntecedent(Markable v) {
    if (MarkablePair_Type.featOkTst && ((MarkablePair_Type)jcasType).casFeat_antecedent == null)
      jcasType.jcas.throwFeatMissing("antecedent", "org.apache.ctakes.coreference.type.MarkablePair");
    jcasType.ll_cas.ll_setRefValue(addr, ((MarkablePair_Type)jcasType).casFeatCode_antecedent, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: anaphor

  /** getter for anaphor - gets 
   * @generated */
  public Markable getAnaphor() {
    if (MarkablePair_Type.featOkTst && ((MarkablePair_Type)jcasType).casFeat_anaphor == null)
      jcasType.jcas.throwFeatMissing("anaphor", "org.apache.ctakes.coreference.type.MarkablePair");
    return (Markable)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((MarkablePair_Type)jcasType).casFeatCode_anaphor)));}
    
  /** setter for anaphor - sets  
   * @generated */
  public void setAnaphor(Markable v) {
    if (MarkablePair_Type.featOkTst && ((MarkablePair_Type)jcasType).casFeat_anaphor == null)
      jcasType.jcas.throwFeatMissing("anaphor", "org.apache.ctakes.coreference.type.MarkablePair");
    jcasType.ll_cas.ll_setRefValue(addr, ((MarkablePair_Type)jcasType).casFeatCode_anaphor, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: label

  /** getter for label - gets Is this pair coreferent?
   * @generated */
  public boolean getLabel() {
    if (MarkablePair_Type.featOkTst && ((MarkablePair_Type)jcasType).casFeat_label == null)
      jcasType.jcas.throwFeatMissing("label", "org.apache.ctakes.coreference.type.MarkablePair");
    return jcasType.ll_cas.ll_getBooleanValue(addr, ((MarkablePair_Type)jcasType).casFeatCode_label);}
    
  /** setter for label - sets Is this pair coreferent? 
   * @generated */
  public void setLabel(boolean v) {
    if (MarkablePair_Type.featOkTst && ((MarkablePair_Type)jcasType).casFeat_label == null)
      jcasType.jcas.throwFeatMissing("label", "org.apache.ctakes.coreference.type.MarkablePair");
    jcasType.ll_cas.ll_setBooleanValue(addr, ((MarkablePair_Type)jcasType).casFeatCode_label, v);}    
  }

    