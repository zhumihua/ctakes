

/* First created by JCasGen Tue Dec 09 23:28:43 EST 2014 */
package org.apache.ctakes.coreference.type;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.cas.FSList;
import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Tue Dec 09 23:28:43 EST 2014
 * XML source: /Users/lusisi/i2b2/apache-ctakes-3.1.1-src/ctakes-coreference/src/main/resources/org/apache/ctakes/coreference/types/TypeSystem.xml
 * @generated */
public class MarkablePairSet extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(MarkablePairSet.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected MarkablePairSet() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public MarkablePairSet(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public MarkablePairSet(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public MarkablePairSet(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: anaphor

  /** getter for anaphor - gets 
   * @generated */
  public Markable getAnaphor() {
    if (MarkablePairSet_Type.featOkTst && ((MarkablePairSet_Type)jcasType).casFeat_anaphor == null)
      jcasType.jcas.throwFeatMissing("anaphor", "org.apache.ctakes.coreference.type.MarkablePairSet");
    return (Markable)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((MarkablePairSet_Type)jcasType).casFeatCode_anaphor)));}
    
  /** setter for anaphor - sets  
   * @generated */
  public void setAnaphor(Markable v) {
    if (MarkablePairSet_Type.featOkTst && ((MarkablePairSet_Type)jcasType).casFeat_anaphor == null)
      jcasType.jcas.throwFeatMissing("anaphor", "org.apache.ctakes.coreference.type.MarkablePairSet");
    jcasType.ll_cas.ll_setRefValue(addr, ((MarkablePairSet_Type)jcasType).casFeatCode_anaphor, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: antecedentList

  /** getter for antecedentList - gets 
   * @generated */
  public FSList getAntecedentList() {
    if (MarkablePairSet_Type.featOkTst && ((MarkablePairSet_Type)jcasType).casFeat_antecedentList == null)
      jcasType.jcas.throwFeatMissing("antecedentList", "org.apache.ctakes.coreference.type.MarkablePairSet");
    return (FSList)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((MarkablePairSet_Type)jcasType).casFeatCode_antecedentList)));}
    
  /** setter for antecedentList - sets  
   * @generated */
  public void setAntecedentList(FSList v) {
    if (MarkablePairSet_Type.featOkTst && ((MarkablePairSet_Type)jcasType).casFeat_antecedentList == null)
      jcasType.jcas.throwFeatMissing("antecedentList", "org.apache.ctakes.coreference.type.MarkablePairSet");
    jcasType.ll_cas.ll_setRefValue(addr, ((MarkablePairSet_Type)jcasType).casFeatCode_antecedentList, jcasType.ll_cas.ll_getFSRef(v));}    
  }

    