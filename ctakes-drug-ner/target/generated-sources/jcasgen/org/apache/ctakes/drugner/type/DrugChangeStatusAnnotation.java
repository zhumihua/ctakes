

/* First created by JCasGen Tue Dec 09 23:28:36 EST 2014 */
package org.apache.ctakes.drugner.type;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** The change status of dosages determination for the Drug NER profile.
 * Updated by JCasGen Tue Dec 09 23:28:36 EST 2014
 * XML source: /Users/lusisi/i2b2/apache-ctakes-3.1.1-src/ctakes-drug-ner/src/main/resources/org/apache/ctakes/drugner/types/TypeSystem.xml
 * @generated */
public class DrugChangeStatusAnnotation extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(DrugChangeStatusAnnotation.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected DrugChangeStatusAnnotation() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public DrugChangeStatusAnnotation(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public DrugChangeStatusAnnotation(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public DrugChangeStatusAnnotation(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: changeStatus

  /** getter for changeStatus - gets Indicates the drug change status of 'stop', 'start', 'increase', 'decrease', or 'noChange'.
   * @generated */
  public String getChangeStatus() {
    if (DrugChangeStatusAnnotation_Type.featOkTst && ((DrugChangeStatusAnnotation_Type)jcasType).casFeat_changeStatus == null)
      jcasType.jcas.throwFeatMissing("changeStatus", "org.apache.ctakes.drugner.type.DrugChangeStatusAnnotation");
    return jcasType.ll_cas.ll_getStringValue(addr, ((DrugChangeStatusAnnotation_Type)jcasType).casFeatCode_changeStatus);}
    
  /** setter for changeStatus - sets Indicates the drug change status of 'stop', 'start', 'increase', 'decrease', or 'noChange'. 
   * @generated */
  public void setChangeStatus(String v) {
    if (DrugChangeStatusAnnotation_Type.featOkTst && ((DrugChangeStatusAnnotation_Type)jcasType).casFeat_changeStatus == null)
      jcasType.jcas.throwFeatMissing("changeStatus", "org.apache.ctakes.drugner.type.DrugChangeStatusAnnotation");
    jcasType.ll_cas.ll_setStringValue(addr, ((DrugChangeStatusAnnotation_Type)jcasType).casFeatCode_changeStatus, v);}    
  }

    