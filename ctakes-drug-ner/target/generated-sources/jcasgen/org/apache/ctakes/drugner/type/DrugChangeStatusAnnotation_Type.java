
/* First created by JCasGen Tue Dec 09 23:28:36 EST 2014 */
package org.apache.ctakes.drugner.type;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** The change status of dosages determination for the Drug NER profile.
 * Updated by JCasGen Tue Dec 09 23:28:36 EST 2014
 * @generated */
public class DrugChangeStatusAnnotation_Type extends Annotation_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (DrugChangeStatusAnnotation_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = DrugChangeStatusAnnotation_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new DrugChangeStatusAnnotation(addr, DrugChangeStatusAnnotation_Type.this);
  			   DrugChangeStatusAnnotation_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new DrugChangeStatusAnnotation(addr, DrugChangeStatusAnnotation_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = DrugChangeStatusAnnotation.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.apache.ctakes.drugner.type.DrugChangeStatusAnnotation");
 
  /** @generated */
  final Feature casFeat_changeStatus;
  /** @generated */
  final int     casFeatCode_changeStatus;
  /** @generated */ 
  public String getChangeStatus(int addr) {
        if (featOkTst && casFeat_changeStatus == null)
      jcas.throwFeatMissing("changeStatus", "org.apache.ctakes.drugner.type.DrugChangeStatusAnnotation");
    return ll_cas.ll_getStringValue(addr, casFeatCode_changeStatus);
  }
  /** @generated */    
  public void setChangeStatus(int addr, String v) {
        if (featOkTst && casFeat_changeStatus == null)
      jcas.throwFeatMissing("changeStatus", "org.apache.ctakes.drugner.type.DrugChangeStatusAnnotation");
    ll_cas.ll_setStringValue(addr, casFeatCode_changeStatus, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public DrugChangeStatusAnnotation_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_changeStatus = jcas.getRequiredFeatureDE(casType, "changeStatus", "uima.cas.String", featOkTst);
    casFeatCode_changeStatus  = (null == casFeat_changeStatus) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_changeStatus).getCode();

  }
}



    