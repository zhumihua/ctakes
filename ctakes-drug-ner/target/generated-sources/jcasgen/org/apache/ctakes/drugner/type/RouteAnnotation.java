

/* First created by JCasGen Tue Dec 09 23:28:36 EST 2014 */
package org.apache.ctakes.drugner.type;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** The route determination for the Drug NER profile.
 * Updated by JCasGen Tue Dec 09 23:28:36 EST 2014
 * XML source: /Users/lusisi/i2b2/apache-ctakes-3.1.1-src/ctakes-drug-ner/src/main/resources/org/apache/ctakes/drugner/types/TypeSystem.xml
 * @generated */
public class RouteAnnotation extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(RouteAnnotation.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected RouteAnnotation() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public RouteAnnotation(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public RouteAnnotation(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public RouteAnnotation(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: intakeMethod

  /** getter for intakeMethod - gets Means by which the drug was taken.
   * @generated */
  public String getIntakeMethod() {
    if (RouteAnnotation_Type.featOkTst && ((RouteAnnotation_Type)jcasType).casFeat_intakeMethod == null)
      jcasType.jcas.throwFeatMissing("intakeMethod", "org.apache.ctakes.drugner.type.RouteAnnotation");
    return jcasType.ll_cas.ll_getStringValue(addr, ((RouteAnnotation_Type)jcasType).casFeatCode_intakeMethod);}
    
  /** setter for intakeMethod - sets Means by which the drug was taken. 
   * @generated */
  public void setIntakeMethod(String v) {
    if (RouteAnnotation_Type.featOkTst && ((RouteAnnotation_Type)jcasType).casFeat_intakeMethod == null)
      jcasType.jcas.throwFeatMissing("intakeMethod", "org.apache.ctakes.drugner.type.RouteAnnotation");
    jcasType.ll_cas.ll_setStringValue(addr, ((RouteAnnotation_Type)jcasType).casFeatCode_intakeMethod, v);}    
  }

    