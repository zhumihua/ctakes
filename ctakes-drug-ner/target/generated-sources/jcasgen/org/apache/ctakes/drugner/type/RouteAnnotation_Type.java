
/* First created by JCasGen Tue Dec 09 23:28:36 EST 2014 */
package org.apache.ctakes.drugner.type;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** The route determination for the Drug NER profile.
 * Updated by JCasGen Tue Dec 09 23:28:36 EST 2014
 * @generated */
public class RouteAnnotation_Type extends Annotation_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (RouteAnnotation_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = RouteAnnotation_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new RouteAnnotation(addr, RouteAnnotation_Type.this);
  			   RouteAnnotation_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new RouteAnnotation(addr, RouteAnnotation_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = RouteAnnotation.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.apache.ctakes.drugner.type.RouteAnnotation");
 
  /** @generated */
  final Feature casFeat_intakeMethod;
  /** @generated */
  final int     casFeatCode_intakeMethod;
  /** @generated */ 
  public String getIntakeMethod(int addr) {
        if (featOkTst && casFeat_intakeMethod == null)
      jcas.throwFeatMissing("intakeMethod", "org.apache.ctakes.drugner.type.RouteAnnotation");
    return ll_cas.ll_getStringValue(addr, casFeatCode_intakeMethod);
  }
  /** @generated */    
  public void setIntakeMethod(int addr, String v) {
        if (featOkTst && casFeat_intakeMethod == null)
      jcas.throwFeatMissing("intakeMethod", "org.apache.ctakes.drugner.type.RouteAnnotation");
    ll_cas.ll_setStringValue(addr, casFeatCode_intakeMethod, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public RouteAnnotation_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_intakeMethod = jcas.getRequiredFeatureDE(casType, "intakeMethod", "uima.cas.String", featOkTst);
    casFeatCode_intakeMethod  = (null == casFeat_intakeMethod) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_intakeMethod).getCode();

  }
}



    