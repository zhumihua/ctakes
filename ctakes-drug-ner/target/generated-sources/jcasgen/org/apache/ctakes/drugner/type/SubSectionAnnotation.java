

/* First created by JCasGen Tue Dec 09 23:28:36 EST 2014 */
package org.apache.ctakes.drugner.type;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Tue Dec 09 23:28:36 EST 2014
 * XML source: /Users/lusisi/i2b2/apache-ctakes-3.1.1-src/ctakes-drug-ner/src/main/resources/org/apache/ctakes/drugner/types/TypeSystem.xml
 * @generated */
public class SubSectionAnnotation extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(SubSectionAnnotation.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected SubSectionAnnotation() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public SubSectionAnnotation(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public SubSectionAnnotation(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public SubSectionAnnotation(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: subSectionBodyBegin

  /** getter for subSectionBodyBegin - gets Sub-section body begin offset.
   * @generated */
  public int getSubSectionBodyBegin() {
    if (SubSectionAnnotation_Type.featOkTst && ((SubSectionAnnotation_Type)jcasType).casFeat_subSectionBodyBegin == null)
      jcasType.jcas.throwFeatMissing("subSectionBodyBegin", "org.apache.ctakes.drugner.type.SubSectionAnnotation");
    return jcasType.ll_cas.ll_getIntValue(addr, ((SubSectionAnnotation_Type)jcasType).casFeatCode_subSectionBodyBegin);}
    
  /** setter for subSectionBodyBegin - sets Sub-section body begin offset. 
   * @generated */
  public void setSubSectionBodyBegin(int v) {
    if (SubSectionAnnotation_Type.featOkTst && ((SubSectionAnnotation_Type)jcasType).casFeat_subSectionBodyBegin == null)
      jcasType.jcas.throwFeatMissing("subSectionBodyBegin", "org.apache.ctakes.drugner.type.SubSectionAnnotation");
    jcasType.ll_cas.ll_setIntValue(addr, ((SubSectionAnnotation_Type)jcasType).casFeatCode_subSectionBodyBegin, v);}    
   
    
  //*--------------*
  //* Feature: subSectionBodyEnd

  /** getter for subSectionBodyEnd - gets Sub-section body end offset.
   * @generated */
  public int getSubSectionBodyEnd() {
    if (SubSectionAnnotation_Type.featOkTst && ((SubSectionAnnotation_Type)jcasType).casFeat_subSectionBodyEnd == null)
      jcasType.jcas.throwFeatMissing("subSectionBodyEnd", "org.apache.ctakes.drugner.type.SubSectionAnnotation");
    return jcasType.ll_cas.ll_getIntValue(addr, ((SubSectionAnnotation_Type)jcasType).casFeatCode_subSectionBodyEnd);}
    
  /** setter for subSectionBodyEnd - sets Sub-section body end offset. 
   * @generated */
  public void setSubSectionBodyEnd(int v) {
    if (SubSectionAnnotation_Type.featOkTst && ((SubSectionAnnotation_Type)jcasType).casFeat_subSectionBodyEnd == null)
      jcasType.jcas.throwFeatMissing("subSectionBodyEnd", "org.apache.ctakes.drugner.type.SubSectionAnnotation");
    jcasType.ll_cas.ll_setIntValue(addr, ((SubSectionAnnotation_Type)jcasType).casFeatCode_subSectionBodyEnd, v);}    
   
    
  //*--------------*
  //* Feature: status

  /** getter for status - gets Status of 'possible', 'history of', or 'family history of'.
   * @generated */
  public int getStatus() {
    if (SubSectionAnnotation_Type.featOkTst && ((SubSectionAnnotation_Type)jcasType).casFeat_status == null)
      jcasType.jcas.throwFeatMissing("status", "org.apache.ctakes.drugner.type.SubSectionAnnotation");
    return jcasType.ll_cas.ll_getIntValue(addr, ((SubSectionAnnotation_Type)jcasType).casFeatCode_status);}
    
  /** setter for status - sets Status of 'possible', 'history of', or 'family history of'. 
   * @generated */
  public void setStatus(int v) {
    if (SubSectionAnnotation_Type.featOkTst && ((SubSectionAnnotation_Type)jcasType).casFeat_status == null)
      jcasType.jcas.throwFeatMissing("status", "org.apache.ctakes.drugner.type.SubSectionAnnotation");
    jcasType.ll_cas.ll_setIntValue(addr, ((SubSectionAnnotation_Type)jcasType).casFeatCode_status, v);}    
   
    
  //*--------------*
  //* Feature: subSectionHeaderBegin

  /** getter for subSectionHeaderBegin - gets Begin offset of subSection header
   * @generated */
  public int getSubSectionHeaderBegin() {
    if (SubSectionAnnotation_Type.featOkTst && ((SubSectionAnnotation_Type)jcasType).casFeat_subSectionHeaderBegin == null)
      jcasType.jcas.throwFeatMissing("subSectionHeaderBegin", "org.apache.ctakes.drugner.type.SubSectionAnnotation");
    return jcasType.ll_cas.ll_getIntValue(addr, ((SubSectionAnnotation_Type)jcasType).casFeatCode_subSectionHeaderBegin);}
    
  /** setter for subSectionHeaderBegin - sets Begin offset of subSection header 
   * @generated */
  public void setSubSectionHeaderBegin(int v) {
    if (SubSectionAnnotation_Type.featOkTst && ((SubSectionAnnotation_Type)jcasType).casFeat_subSectionHeaderBegin == null)
      jcasType.jcas.throwFeatMissing("subSectionHeaderBegin", "org.apache.ctakes.drugner.type.SubSectionAnnotation");
    jcasType.ll_cas.ll_setIntValue(addr, ((SubSectionAnnotation_Type)jcasType).casFeatCode_subSectionHeaderBegin, v);}    
   
    
  //*--------------*
  //* Feature: subSectionHeaderEnd

  /** getter for subSectionHeaderEnd - gets Ending offset of subsection header
   * @generated */
  public int getSubSectionHeaderEnd() {
    if (SubSectionAnnotation_Type.featOkTst && ((SubSectionAnnotation_Type)jcasType).casFeat_subSectionHeaderEnd == null)
      jcasType.jcas.throwFeatMissing("subSectionHeaderEnd", "org.apache.ctakes.drugner.type.SubSectionAnnotation");
    return jcasType.ll_cas.ll_getIntValue(addr, ((SubSectionAnnotation_Type)jcasType).casFeatCode_subSectionHeaderEnd);}
    
  /** setter for subSectionHeaderEnd - sets Ending offset of subsection header 
   * @generated */
  public void setSubSectionHeaderEnd(int v) {
    if (SubSectionAnnotation_Type.featOkTst && ((SubSectionAnnotation_Type)jcasType).casFeat_subSectionHeaderEnd == null)
      jcasType.jcas.throwFeatMissing("subSectionHeaderEnd", "org.apache.ctakes.drugner.type.SubSectionAnnotation");
    jcasType.ll_cas.ll_setIntValue(addr, ((SubSectionAnnotation_Type)jcasType).casFeatCode_subSectionHeaderEnd, v);}    
   
    
  //*--------------*
  //* Feature: parentSectionId

  /** getter for parentSectionId - gets The section in which the subsection was found.
   * @generated */
  public String getParentSectionId() {
    if (SubSectionAnnotation_Type.featOkTst && ((SubSectionAnnotation_Type)jcasType).casFeat_parentSectionId == null)
      jcasType.jcas.throwFeatMissing("parentSectionId", "org.apache.ctakes.drugner.type.SubSectionAnnotation");
    return jcasType.ll_cas.ll_getStringValue(addr, ((SubSectionAnnotation_Type)jcasType).casFeatCode_parentSectionId);}
    
  /** setter for parentSectionId - sets The section in which the subsection was found. 
   * @generated */
  public void setParentSectionId(String v) {
    if (SubSectionAnnotation_Type.featOkTst && ((SubSectionAnnotation_Type)jcasType).casFeat_parentSectionId == null)
      jcasType.jcas.throwFeatMissing("parentSectionId", "org.apache.ctakes.drugner.type.SubSectionAnnotation");
    jcasType.ll_cas.ll_setStringValue(addr, ((SubSectionAnnotation_Type)jcasType).casFeatCode_parentSectionId, v);}    
  }

    