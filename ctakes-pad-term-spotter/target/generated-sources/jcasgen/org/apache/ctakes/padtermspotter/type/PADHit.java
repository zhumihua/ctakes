

/* First created by JCasGen Tue Dec 09 23:28:34 EST 2014 */
package org.apache.ctakes.padtermspotter.type;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.ctakes.typesystem.type.textsem.IdentifiedAnnotation;


/** Would represent one of the following
				Term (in the case of a stand alone term)
				Term + Location
				Location + term
 * Updated by JCasGen Tue Dec 09 23:28:34 EST 2014
 * XML source: /Users/lusisi/i2b2/apache-ctakes-3.1.1-src/ctakes-pad-term-spotter/src/main/resources/org/apache/ctakes/padtermspotter/types/TypeSystem.xml
 * @generated */
public class PADHit extends IdentifiedAnnotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(PADHit.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected PADHit() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public PADHit(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public PADHit(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public PADHit(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: uaTerm

  /** getter for uaTerm - gets 
   * @generated */
  public PADTerm getUaTerm() {
    if (PADHit_Type.featOkTst && ((PADHit_Type)jcasType).casFeat_uaTerm == null)
      jcasType.jcas.throwFeatMissing("uaTerm", "org.apache.ctakes.padtermspotter.type.PADHit");
    return (PADTerm)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((PADHit_Type)jcasType).casFeatCode_uaTerm)));}
    
  /** setter for uaTerm - sets  
   * @generated */
  public void setUaTerm(PADTerm v) {
    if (PADHit_Type.featOkTst && ((PADHit_Type)jcasType).casFeat_uaTerm == null)
      jcasType.jcas.throwFeatMissing("uaTerm", "org.apache.ctakes.padtermspotter.type.PADHit");
    jcasType.ll_cas.ll_setRefValue(addr, ((PADHit_Type)jcasType).casFeatCode_uaTerm, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: uaLocation

  /** getter for uaLocation - gets 
   * @generated */
  public PADLocation getUaLocation() {
    if (PADHit_Type.featOkTst && ((PADHit_Type)jcasType).casFeat_uaLocation == null)
      jcasType.jcas.throwFeatMissing("uaLocation", "org.apache.ctakes.padtermspotter.type.PADHit");
    return (PADLocation)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((PADHit_Type)jcasType).casFeatCode_uaLocation)));}
    
  /** setter for uaLocation - sets  
   * @generated */
  public void setUaLocation(PADLocation v) {
    if (PADHit_Type.featOkTst && ((PADHit_Type)jcasType).casFeat_uaLocation == null)
      jcasType.jcas.throwFeatMissing("uaLocation", "org.apache.ctakes.padtermspotter.type.PADHit");
    jcasType.ll_cas.ll_setRefValue(addr, ((PADHit_Type)jcasType).casFeatCode_uaLocation, jcasType.ll_cas.ll_getFSRef(v));}    
  }

    