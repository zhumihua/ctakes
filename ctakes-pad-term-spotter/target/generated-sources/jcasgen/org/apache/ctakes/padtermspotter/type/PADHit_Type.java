
/* First created by JCasGen Tue Dec 09 23:28:34 EST 2014 */
package org.apache.ctakes.padtermspotter.type;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.ctakes.typesystem.type.textsem.IdentifiedAnnotation_Type;

/** Would represent one of the following
				Term (in the case of a stand alone term)
				Term + Location
				Location + term
 * Updated by JCasGen Tue Dec 09 23:28:34 EST 2014
 * @generated */
public class PADHit_Type extends IdentifiedAnnotation_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (PADHit_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = PADHit_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new PADHit(addr, PADHit_Type.this);
  			   PADHit_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new PADHit(addr, PADHit_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = PADHit.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.apache.ctakes.padtermspotter.type.PADHit");
 
  /** @generated */
  final Feature casFeat_uaTerm;
  /** @generated */
  final int     casFeatCode_uaTerm;
  /** @generated */ 
  public int getUaTerm(int addr) {
        if (featOkTst && casFeat_uaTerm == null)
      jcas.throwFeatMissing("uaTerm", "org.apache.ctakes.padtermspotter.type.PADHit");
    return ll_cas.ll_getRefValue(addr, casFeatCode_uaTerm);
  }
  /** @generated */    
  public void setUaTerm(int addr, int v) {
        if (featOkTst && casFeat_uaTerm == null)
      jcas.throwFeatMissing("uaTerm", "org.apache.ctakes.padtermspotter.type.PADHit");
    ll_cas.ll_setRefValue(addr, casFeatCode_uaTerm, v);}
    
  
 
  /** @generated */
  final Feature casFeat_uaLocation;
  /** @generated */
  final int     casFeatCode_uaLocation;
  /** @generated */ 
  public int getUaLocation(int addr) {
        if (featOkTst && casFeat_uaLocation == null)
      jcas.throwFeatMissing("uaLocation", "org.apache.ctakes.padtermspotter.type.PADHit");
    return ll_cas.ll_getRefValue(addr, casFeatCode_uaLocation);
  }
  /** @generated */    
  public void setUaLocation(int addr, int v) {
        if (featOkTst && casFeat_uaLocation == null)
      jcas.throwFeatMissing("uaLocation", "org.apache.ctakes.padtermspotter.type.PADHit");
    ll_cas.ll_setRefValue(addr, casFeatCode_uaLocation, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public PADHit_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_uaTerm = jcas.getRequiredFeatureDE(casType, "uaTerm", "org.apache.ctakes.padtermspotter.type.PADTerm", featOkTst);
    casFeatCode_uaTerm  = (null == casFeat_uaTerm) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_uaTerm).getCode();

 
    casFeat_uaLocation = jcas.getRequiredFeatureDE(casType, "uaLocation", "org.apache.ctakes.padtermspotter.type.PADLocation", featOkTst);
    casFeatCode_uaLocation  = (null == casFeat_uaLocation) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_uaLocation).getCode();

  }
}



    