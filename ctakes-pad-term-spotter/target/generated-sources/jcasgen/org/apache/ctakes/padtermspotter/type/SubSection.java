

/* First created by JCasGen Tue Dec 09 23:28:34 EST 2014 */
package org.apache.ctakes.padtermspotter.type;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Tue Dec 09 23:28:34 EST 2014
 * XML source: /Users/lusisi/i2b2/apache-ctakes-3.1.1-src/ctakes-pad-term-spotter/src/main/resources/org/apache/ctakes/padtermspotter/types/TypeSystem.xml
 * @generated */
public class SubSection extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(SubSection.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected SubSection() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public SubSection(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public SubSection(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public SubSection(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: parentSectionId

  /** getter for parentSectionId - gets 
   * @generated */
  public String getParentSectionId() {
    if (SubSection_Type.featOkTst && ((SubSection_Type)jcasType).casFeat_parentSectionId == null)
      jcasType.jcas.throwFeatMissing("parentSectionId", "org.apache.ctakes.padtermspotter.type.SubSection");
    return jcasType.ll_cas.ll_getStringValue(addr, ((SubSection_Type)jcasType).casFeatCode_parentSectionId);}
    
  /** setter for parentSectionId - sets  
   * @generated */
  public void setParentSectionId(String v) {
    if (SubSection_Type.featOkTst && ((SubSection_Type)jcasType).casFeat_parentSectionId == null)
      jcasType.jcas.throwFeatMissing("parentSectionId", "org.apache.ctakes.padtermspotter.type.SubSection");
    jcasType.ll_cas.ll_setStringValue(addr, ((SubSection_Type)jcasType).casFeatCode_parentSectionId, v);}    
   
    
  //*--------------*
  //* Feature: subSectionBodyBegin

  /** getter for subSectionBodyBegin - gets 
   * @generated */
  public int getSubSectionBodyBegin() {
    if (SubSection_Type.featOkTst && ((SubSection_Type)jcasType).casFeat_subSectionBodyBegin == null)
      jcasType.jcas.throwFeatMissing("subSectionBodyBegin", "org.apache.ctakes.padtermspotter.type.SubSection");
    return jcasType.ll_cas.ll_getIntValue(addr, ((SubSection_Type)jcasType).casFeatCode_subSectionBodyBegin);}
    
  /** setter for subSectionBodyBegin - sets  
   * @generated */
  public void setSubSectionBodyBegin(int v) {
    if (SubSection_Type.featOkTst && ((SubSection_Type)jcasType).casFeat_subSectionBodyBegin == null)
      jcasType.jcas.throwFeatMissing("subSectionBodyBegin", "org.apache.ctakes.padtermspotter.type.SubSection");
    jcasType.ll_cas.ll_setIntValue(addr, ((SubSection_Type)jcasType).casFeatCode_subSectionBodyBegin, v);}    
   
    
  //*--------------*
  //* Feature: subSectionBodyEnd

  /** getter for subSectionBodyEnd - gets 
   * @generated */
  public int getSubSectionBodyEnd() {
    if (SubSection_Type.featOkTst && ((SubSection_Type)jcasType).casFeat_subSectionBodyEnd == null)
      jcasType.jcas.throwFeatMissing("subSectionBodyEnd", "org.apache.ctakes.padtermspotter.type.SubSection");
    return jcasType.ll_cas.ll_getIntValue(addr, ((SubSection_Type)jcasType).casFeatCode_subSectionBodyEnd);}
    
  /** setter for subSectionBodyEnd - sets  
   * @generated */
  public void setSubSectionBodyEnd(int v) {
    if (SubSection_Type.featOkTst && ((SubSection_Type)jcasType).casFeat_subSectionBodyEnd == null)
      jcasType.jcas.throwFeatMissing("subSectionBodyEnd", "org.apache.ctakes.padtermspotter.type.SubSection");
    jcasType.ll_cas.ll_setIntValue(addr, ((SubSection_Type)jcasType).casFeatCode_subSectionBodyEnd, v);}    
   
    
  //*--------------*
  //* Feature: status

  /** getter for status - gets 
   * @generated */
  public int getStatus() {
    if (SubSection_Type.featOkTst && ((SubSection_Type)jcasType).casFeat_status == null)
      jcasType.jcas.throwFeatMissing("status", "org.apache.ctakes.padtermspotter.type.SubSection");
    return jcasType.ll_cas.ll_getIntValue(addr, ((SubSection_Type)jcasType).casFeatCode_status);}
    
  /** setter for status - sets  
   * @generated */
  public void setStatus(int v) {
    if (SubSection_Type.featOkTst && ((SubSection_Type)jcasType).casFeat_status == null)
      jcasType.jcas.throwFeatMissing("status", "org.apache.ctakes.padtermspotter.type.SubSection");
    jcasType.ll_cas.ll_setIntValue(addr, ((SubSection_Type)jcasType).casFeatCode_status, v);}    
   
    
  //*--------------*
  //* Feature: subSectionHeaderBegin

  /** getter for subSectionHeaderBegin - gets 
   * @generated */
  public int getSubSectionHeaderBegin() {
    if (SubSection_Type.featOkTst && ((SubSection_Type)jcasType).casFeat_subSectionHeaderBegin == null)
      jcasType.jcas.throwFeatMissing("subSectionHeaderBegin", "org.apache.ctakes.padtermspotter.type.SubSection");
    return jcasType.ll_cas.ll_getIntValue(addr, ((SubSection_Type)jcasType).casFeatCode_subSectionHeaderBegin);}
    
  /** setter for subSectionHeaderBegin - sets  
   * @generated */
  public void setSubSectionHeaderBegin(int v) {
    if (SubSection_Type.featOkTst && ((SubSection_Type)jcasType).casFeat_subSectionHeaderBegin == null)
      jcasType.jcas.throwFeatMissing("subSectionHeaderBegin", "org.apache.ctakes.padtermspotter.type.SubSection");
    jcasType.ll_cas.ll_setIntValue(addr, ((SubSection_Type)jcasType).casFeatCode_subSectionHeaderBegin, v);}    
   
    
  //*--------------*
  //* Feature: subSectionHeaderEnd

  /** getter for subSectionHeaderEnd - gets 
   * @generated */
  public int getSubSectionHeaderEnd() {
    if (SubSection_Type.featOkTst && ((SubSection_Type)jcasType).casFeat_subSectionHeaderEnd == null)
      jcasType.jcas.throwFeatMissing("subSectionHeaderEnd", "org.apache.ctakes.padtermspotter.type.SubSection");
    return jcasType.ll_cas.ll_getIntValue(addr, ((SubSection_Type)jcasType).casFeatCode_subSectionHeaderEnd);}
    
  /** setter for subSectionHeaderEnd - sets  
   * @generated */
  public void setSubSectionHeaderEnd(int v) {
    if (SubSection_Type.featOkTst && ((SubSection_Type)jcasType).casFeat_subSectionHeaderEnd == null)
      jcasType.jcas.throwFeatMissing("subSectionHeaderEnd", "org.apache.ctakes.padtermspotter.type.SubSection");
    jcasType.ll_cas.ll_setIntValue(addr, ((SubSection_Type)jcasType).casFeatCode_subSectionHeaderEnd, v);}    
  }

    