
/* First created by JCasGen Tue Dec 09 23:28:34 EST 2014 */
package org.apache.ctakes.typesystem.type.i2b2;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Tue Dec 09 23:28:34 EST 2014
 * @generated */
public class Disease_Type extends tag_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Disease_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Disease_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Disease(addr, Disease_Type.this);
  			   Disease_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Disease(addr, Disease_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Disease.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.apache.ctakes.typesystem.type.i2b2.Disease");
 
  /** @generated */
  final Feature casFeat_indicator;
  /** @generated */
  final int     casFeatCode_indicator;
  /** @generated */ 
  public String getIndicator(int addr) {
        if (featOkTst && casFeat_indicator == null)
      jcas.throwFeatMissing("indicator", "org.apache.ctakes.typesystem.type.i2b2.Disease");
    return ll_cas.ll_getStringValue(addr, casFeatCode_indicator);
  }
  /** @generated */    
  public void setIndicator(int addr, String v) {
        if (featOkTst && casFeat_indicator == null)
      jcas.throwFeatMissing("indicator", "org.apache.ctakes.typesystem.type.i2b2.Disease");
    ll_cas.ll_setStringValue(addr, casFeatCode_indicator, v);}
    
  
 
  /** @generated */
  final Feature casFeat_time;
  /** @generated */
  final int     casFeatCode_time;
  /** @generated */ 
  public String getTime(int addr) {
        if (featOkTst && casFeat_time == null)
      jcas.throwFeatMissing("time", "org.apache.ctakes.typesystem.type.i2b2.Disease");
    return ll_cas.ll_getStringValue(addr, casFeatCode_time);
  }
  /** @generated */    
  public void setTime(int addr, String v) {
        if (featOkTst && casFeat_time == null)
      jcas.throwFeatMissing("time", "org.apache.ctakes.typesystem.type.i2b2.Disease");
    ll_cas.ll_setStringValue(addr, casFeatCode_time, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public Disease_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_indicator = jcas.getRequiredFeatureDE(casType, "indicator", "uima.cas.String", featOkTst);
    casFeatCode_indicator  = (null == casFeat_indicator) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_indicator).getCode();

 
    casFeat_time = jcas.getRequiredFeatureDE(casType, "time", "uima.cas.String", featOkTst);
    casFeatCode_time  = (null == casFeat_time) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_time).getCode();

  }
}



    