
/* First created by JCasGen Tue Dec 09 23:28:34 EST 2014 */
package org.apache.ctakes.typesystem.type.i2b2;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Tue Dec 09 23:28:34 EST 2014
 * @generated */
public class GoldMedication_Type extends GoldTag_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (GoldMedication_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = GoldMedication_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new GoldMedication(addr, GoldMedication_Type.this);
  			   GoldMedication_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new GoldMedication(addr, GoldMedication_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = GoldMedication.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.apache.ctakes.typesystem.type.i2b2.GoldMedication");
 
  /** @generated */
  final Feature casFeat_type1;
  /** @generated */
  final int     casFeatCode_type1;
  /** @generated */ 
  public String getType1(int addr) {
        if (featOkTst && casFeat_type1 == null)
      jcas.throwFeatMissing("type1", "org.apache.ctakes.typesystem.type.i2b2.GoldMedication");
    return ll_cas.ll_getStringValue(addr, casFeatCode_type1);
  }
  /** @generated */    
  public void setType1(int addr, String v) {
        if (featOkTst && casFeat_type1 == null)
      jcas.throwFeatMissing("type1", "org.apache.ctakes.typesystem.type.i2b2.GoldMedication");
    ll_cas.ll_setStringValue(addr, casFeatCode_type1, v);}
    
  
 
  /** @generated */
  final Feature casFeat_type2;
  /** @generated */
  final int     casFeatCode_type2;
  /** @generated */ 
  public String getType2(int addr) {
        if (featOkTst && casFeat_type2 == null)
      jcas.throwFeatMissing("type2", "org.apache.ctakes.typesystem.type.i2b2.GoldMedication");
    return ll_cas.ll_getStringValue(addr, casFeatCode_type2);
  }
  /** @generated */    
  public void setType2(int addr, String v) {
        if (featOkTst && casFeat_type2 == null)
      jcas.throwFeatMissing("type2", "org.apache.ctakes.typesystem.type.i2b2.GoldMedication");
    ll_cas.ll_setStringValue(addr, casFeatCode_type2, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public GoldMedication_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_type1 = jcas.getRequiredFeatureDE(casType, "type1", "uima.cas.String", featOkTst);
    casFeatCode_type1  = (null == casFeat_type1) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_type1).getCode();

 
    casFeat_type2 = jcas.getRequiredFeatureDE(casType, "type2", "uima.cas.String", featOkTst);
    casFeatCode_type2  = (null == casFeat_type2) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_type2).getCode();

  }
}



    