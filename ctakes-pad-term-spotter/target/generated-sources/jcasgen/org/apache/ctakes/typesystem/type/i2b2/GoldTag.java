

/* First created by JCasGen Tue Dec 09 23:28:34 EST 2014 */
package org.apache.ctakes.typesystem.type.i2b2;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Tue Dec 09 23:28:34 EST 2014
 * XML source: /Users/lusisi/i2b2/apache-ctakes-3.1.1-src/ctakes-pad-term-spotter/src/main/resources/org/apache/ctakes/padtermspotter/types/TypeSystem.xml
 * @generated */
public class GoldTag extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(GoldTag.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected GoldTag() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public GoldTag(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public GoldTag(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public GoldTag(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: tagName

  /** getter for tagName - gets the element name
   * @generated */
  public String getTagName() {
    if (GoldTag_Type.featOkTst && ((GoldTag_Type)jcasType).casFeat_tagName == null)
      jcasType.jcas.throwFeatMissing("tagName", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    return jcasType.ll_cas.ll_getStringValue(addr, ((GoldTag_Type)jcasType).casFeatCode_tagName);}
    
  /** setter for tagName - sets the element name 
   * @generated */
  public void setTagName(String v) {
    if (GoldTag_Type.featOkTst && ((GoldTag_Type)jcasType).casFeat_tagName == null)
      jcasType.jcas.throwFeatMissing("tagName", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    jcasType.ll_cas.ll_setStringValue(addr, ((GoldTag_Type)jcasType).casFeatCode_tagName, v);}    
   
    
  //*--------------*
  //* Feature: isMed

  /** getter for isMed - gets 
   * @generated */
  public boolean getIsMed() {
    if (GoldTag_Type.featOkTst && ((GoldTag_Type)jcasType).casFeat_isMed == null)
      jcasType.jcas.throwFeatMissing("isMed", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    return jcasType.ll_cas.ll_getBooleanValue(addr, ((GoldTag_Type)jcasType).casFeatCode_isMed);}
    
  /** setter for isMed - sets  
   * @generated */
  public void setIsMed(boolean v) {
    if (GoldTag_Type.featOkTst && ((GoldTag_Type)jcasType).casFeat_isMed == null)
      jcasType.jcas.throwFeatMissing("isMed", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    jcasType.ll_cas.ll_setBooleanValue(addr, ((GoldTag_Type)jcasType).casFeatCode_isMed, v);}    
   
    
  //*--------------*
  //* Feature: sectName

  /** getter for sectName - gets 
   * @generated */
  public String getSectName() {
    if (GoldTag_Type.featOkTst && ((GoldTag_Type)jcasType).casFeat_sectName == null)
      jcasType.jcas.throwFeatMissing("sectName", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    return jcasType.ll_cas.ll_getStringValue(addr, ((GoldTag_Type)jcasType).casFeatCode_sectName);}
    
  /** setter for sectName - sets  
   * @generated */
  public void setSectName(String v) {
    if (GoldTag_Type.featOkTst && ((GoldTag_Type)jcasType).casFeat_sectName == null)
      jcasType.jcas.throwFeatMissing("sectName", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    jcasType.ll_cas.ll_setStringValue(addr, ((GoldTag_Type)jcasType).casFeatCode_sectName, v);}    
   
    
  //*--------------*
  //* Feature: annoText

  /** getter for annoText - gets 
   * @generated */
  public String getAnnoText() {
    if (GoldTag_Type.featOkTst && ((GoldTag_Type)jcasType).casFeat_annoText == null)
      jcasType.jcas.throwFeatMissing("annoText", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    return jcasType.ll_cas.ll_getStringValue(addr, ((GoldTag_Type)jcasType).casFeatCode_annoText);}
    
  /** setter for annoText - sets  
   * @generated */
  public void setAnnoText(String v) {
    if (GoldTag_Type.featOkTst && ((GoldTag_Type)jcasType).casFeat_annoText == null)
      jcasType.jcas.throwFeatMissing("annoText", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    jcasType.ll_cas.ll_setStringValue(addr, ((GoldTag_Type)jcasType).casFeatCode_annoText, v);}    
   
    
  //*--------------*
  //* Feature: time_after

  /** getter for time_after - gets 
   * @generated */
  public int getTime_after() {
    if (GoldTag_Type.featOkTst && ((GoldTag_Type)jcasType).casFeat_time_after == null)
      jcasType.jcas.throwFeatMissing("time_after", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    return jcasType.ll_cas.ll_getIntValue(addr, ((GoldTag_Type)jcasType).casFeatCode_time_after);}
    
  /** setter for time_after - sets  
   * @generated */
  public void setTime_after(int v) {
    if (GoldTag_Type.featOkTst && ((GoldTag_Type)jcasType).casFeat_time_after == null)
      jcasType.jcas.throwFeatMissing("time_after", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    jcasType.ll_cas.ll_setIntValue(addr, ((GoldTag_Type)jcasType).casFeatCode_time_after, v);}    
   
    
  //*--------------*
  //* Feature: time_before

  /** getter for time_before - gets 
   * @generated */
  public int getTime_before() {
    if (GoldTag_Type.featOkTst && ((GoldTag_Type)jcasType).casFeat_time_before == null)
      jcasType.jcas.throwFeatMissing("time_before", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    return jcasType.ll_cas.ll_getIntValue(addr, ((GoldTag_Type)jcasType).casFeatCode_time_before);}
    
  /** setter for time_before - sets  
   * @generated */
  public void setTime_before(int v) {
    if (GoldTag_Type.featOkTst && ((GoldTag_Type)jcasType).casFeat_time_before == null)
      jcasType.jcas.throwFeatMissing("time_before", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    jcasType.ll_cas.ll_setIntValue(addr, ((GoldTag_Type)jcasType).casFeatCode_time_before, v);}    
   
    
  //*--------------*
  //* Feature: time_during

  /** getter for time_during - gets 
   * @generated */
  public int getTime_during() {
    if (GoldTag_Type.featOkTst && ((GoldTag_Type)jcasType).casFeat_time_during == null)
      jcasType.jcas.throwFeatMissing("time_during", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    return jcasType.ll_cas.ll_getIntValue(addr, ((GoldTag_Type)jcasType).casFeatCode_time_during);}
    
  /** setter for time_during - sets  
   * @generated */
  public void setTime_during(int v) {
    if (GoldTag_Type.featOkTst && ((GoldTag_Type)jcasType).casFeat_time_during == null)
      jcasType.jcas.throwFeatMissing("time_during", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    jcasType.ll_cas.ll_setIntValue(addr, ((GoldTag_Type)jcasType).casFeatCode_time_during, v);}    
   
    
  //*--------------*
  //* Feature: time_value

  /** getter for time_value - gets the time value combination: before, during, after
   * @generated */
  public int getTime_value() {
    if (GoldTag_Type.featOkTst && ((GoldTag_Type)jcasType).casFeat_time_value == null)
      jcasType.jcas.throwFeatMissing("time_value", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    return jcasType.ll_cas.ll_getIntValue(addr, ((GoldTag_Type)jcasType).casFeatCode_time_value);}
    
  /** setter for time_value - sets the time value combination: before, during, after 
   * @generated */
  public void setTime_value(int v) {
    if (GoldTag_Type.featOkTst && ((GoldTag_Type)jcasType).casFeat_time_value == null)
      jcasType.jcas.throwFeatMissing("time_value", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    jcasType.ll_cas.ll_setIntValue(addr, ((GoldTag_Type)jcasType).casFeatCode_time_value, v);}    
   
    
  //*--------------*
  //* Feature: dct

  /** getter for dct - gets 
   * @generated */
  public String getDct() {
    if (GoldTag_Type.featOkTst && ((GoldTag_Type)jcasType).casFeat_dct == null)
      jcasType.jcas.throwFeatMissing("dct", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    return jcasType.ll_cas.ll_getStringValue(addr, ((GoldTag_Type)jcasType).casFeatCode_dct);}
    
  /** setter for dct - sets  
   * @generated */
  public void setDct(String v) {
    if (GoldTag_Type.featOkTst && ((GoldTag_Type)jcasType).casFeat_dct == null)
      jcasType.jcas.throwFeatMissing("dct", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    jcasType.ll_cas.ll_setStringValue(addr, ((GoldTag_Type)jcasType).casFeatCode_dct, v);}    
   
    
  //*--------------*
  //* Feature: annoStart

  /** getter for annoStart - gets 
   * @generated */
  public int getAnnoStart() {
    if (GoldTag_Type.featOkTst && ((GoldTag_Type)jcasType).casFeat_annoStart == null)
      jcasType.jcas.throwFeatMissing("annoStart", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    return jcasType.ll_cas.ll_getIntValue(addr, ((GoldTag_Type)jcasType).casFeatCode_annoStart);}
    
  /** setter for annoStart - sets  
   * @generated */
  public void setAnnoStart(int v) {
    if (GoldTag_Type.featOkTst && ((GoldTag_Type)jcasType).casFeat_annoStart == null)
      jcasType.jcas.throwFeatMissing("annoStart", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    jcasType.ll_cas.ll_setIntValue(addr, ((GoldTag_Type)jcasType).casFeatCode_annoStart, v);}    
   
    
  //*--------------*
  //* Feature: annoEnd

  /** getter for annoEnd - gets 
   * @generated */
  public int getAnnoEnd() {
    if (GoldTag_Type.featOkTst && ((GoldTag_Type)jcasType).casFeat_annoEnd == null)
      jcasType.jcas.throwFeatMissing("annoEnd", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    return jcasType.ll_cas.ll_getIntValue(addr, ((GoldTag_Type)jcasType).casFeatCode_annoEnd);}
    
  /** setter for annoEnd - sets  
   * @generated */
  public void setAnnoEnd(int v) {
    if (GoldTag_Type.featOkTst && ((GoldTag_Type)jcasType).casFeat_annoEnd == null)
      jcasType.jcas.throwFeatMissing("annoEnd", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    jcasType.ll_cas.ll_setIntValue(addr, ((GoldTag_Type)jcasType).casFeatCode_annoEnd, v);}    
  }

    