

/* First created by JCasGen Tue Dec 09 23:28:35 EST 2014 */
package org.apache.ctakes.typesystem.type.textsem;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** An indicator to warn that the laboratory test result has
				changed significantly from the previous identical laboratory test
				result.
 * Updated by JCasGen Tue Dec 09 23:28:35 EST 2014
 * XML source: /Users/lusisi/i2b2/apache-ctakes-3.1.1-src/ctakes-pad-term-spotter/src/main/resources/org/apache/ctakes/padtermspotter/types/TypeSystem.xml
 * @generated */
public class LabDeltaFlagModifier extends Modifier {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(LabDeltaFlagModifier.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected LabDeltaFlagModifier() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public LabDeltaFlagModifier(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public LabDeltaFlagModifier(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public LabDeltaFlagModifier(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
}

    