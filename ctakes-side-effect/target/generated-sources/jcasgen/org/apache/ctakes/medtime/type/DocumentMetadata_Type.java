
/* First created by JCasGen Tue Dec 09 23:28:38 EST 2014 */
package org.apache.ctakes.medtime.type;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** Structured data that captures information about the document, patient, or context of the clinical text.
 * Updated by JCasGen Tue Dec 09 23:28:38 EST 2014
 * @generated */
public class DocumentMetadata_Type extends Annotation_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (DocumentMetadata_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = DocumentMetadata_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new DocumentMetadata(addr, DocumentMetadata_Type.this);
  			   DocumentMetadata_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new DocumentMetadata(addr, DocumentMetadata_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = DocumentMetadata.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.apache.ctakes.medtime.type.DocumentMetadata");
 
  /** @generated */
  final Feature casFeat_fileName;
  /** @generated */
  final int     casFeatCode_fileName;
  /** @generated */ 
  public String getFileName(int addr) {
        if (featOkTst && casFeat_fileName == null)
      jcas.throwFeatMissing("fileName", "org.apache.ctakes.medtime.type.DocumentMetadata");
    return ll_cas.ll_getStringValue(addr, casFeatCode_fileName);
  }
  /** @generated */    
  public void setFileName(int addr, String v) {
        if (featOkTst && casFeat_fileName == null)
      jcas.throwFeatMissing("fileName", "org.apache.ctakes.medtime.type.DocumentMetadata");
    ll_cas.ll_setStringValue(addr, casFeatCode_fileName, v);}
    
  
 
  /** @generated */
  final Feature casFeat_timexId;
  /** @generated */
  final int     casFeatCode_timexId;
  /** @generated */ 
  public String getTimexId(int addr) {
        if (featOkTst && casFeat_timexId == null)
      jcas.throwFeatMissing("timexId", "org.apache.ctakes.medtime.type.DocumentMetadata");
    return ll_cas.ll_getStringValue(addr, casFeatCode_timexId);
  }
  /** @generated */    
  public void setTimexId(int addr, String v) {
        if (featOkTst && casFeat_timexId == null)
      jcas.throwFeatMissing("timexId", "org.apache.ctakes.medtime.type.DocumentMetadata");
    ll_cas.ll_setStringValue(addr, casFeatCode_timexId, v);}
    
  
 
  /** @generated */
  final Feature casFeat_timexValue;
  /** @generated */
  final int     casFeatCode_timexValue;
  /** @generated */ 
  public String getTimexValue(int addr) {
        if (featOkTst && casFeat_timexValue == null)
      jcas.throwFeatMissing("timexValue", "org.apache.ctakes.medtime.type.DocumentMetadata");
    return ll_cas.ll_getStringValue(addr, casFeatCode_timexValue);
  }
  /** @generated */    
  public void setTimexValue(int addr, String v) {
        if (featOkTst && casFeat_timexValue == null)
      jcas.throwFeatMissing("timexValue", "org.apache.ctakes.medtime.type.DocumentMetadata");
    ll_cas.ll_setStringValue(addr, casFeatCode_timexValue, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public DocumentMetadata_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_fileName = jcas.getRequiredFeatureDE(casType, "fileName", "uima.cas.String", featOkTst);
    casFeatCode_fileName  = (null == casFeat_fileName) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_fileName).getCode();

 
    casFeat_timexId = jcas.getRequiredFeatureDE(casType, "timexId", "uima.cas.String", featOkTst);
    casFeatCode_timexId  = (null == casFeat_timexId) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_timexId).getCode();

 
    casFeat_timexValue = jcas.getRequiredFeatureDE(casType, "timexValue", "uima.cas.String", featOkTst);
    casFeatCode_timexValue  = (null == casFeat_timexValue) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_timexValue).getCode();

  }
}



    