

/* First created by JCasGen Tue Dec 09 23:28:38 EST 2014 */
package org.apache.ctakes.typesystem.type.i2b2;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Tue Dec 09 23:28:38 EST 2014
 * XML source: /Users/lusisi/i2b2/apache-ctakes-3.1.1-src/ctakes-side-effect/src/main/resources/org/apache/ctakes/sideeffect/types/TypeSystem.xml
 * @generated */
public class Disease extends tag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Disease.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Disease() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Disease(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Disease(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Disease(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: indicator

  /** getter for indicator - gets 
   * @generated */
  public String getIndicator() {
    if (Disease_Type.featOkTst && ((Disease_Type)jcasType).casFeat_indicator == null)
      jcasType.jcas.throwFeatMissing("indicator", "org.apache.ctakes.typesystem.type.i2b2.Disease");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Disease_Type)jcasType).casFeatCode_indicator);}
    
  /** setter for indicator - sets  
   * @generated */
  public void setIndicator(String v) {
    if (Disease_Type.featOkTst && ((Disease_Type)jcasType).casFeat_indicator == null)
      jcasType.jcas.throwFeatMissing("indicator", "org.apache.ctakes.typesystem.type.i2b2.Disease");
    jcasType.ll_cas.ll_setStringValue(addr, ((Disease_Type)jcasType).casFeatCode_indicator, v);}    
   
    
  //*--------------*
  //* Feature: time

  /** getter for time - gets 
   * @generated */
  public String getTime() {
    if (Disease_Type.featOkTst && ((Disease_Type)jcasType).casFeat_time == null)
      jcasType.jcas.throwFeatMissing("time", "org.apache.ctakes.typesystem.type.i2b2.Disease");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Disease_Type)jcasType).casFeatCode_time);}
    
  /** setter for time - sets  
   * @generated */
  public void setTime(String v) {
    if (Disease_Type.featOkTst && ((Disease_Type)jcasType).casFeat_time == null)
      jcasType.jcas.throwFeatMissing("time", "org.apache.ctakes.typesystem.type.i2b2.Disease");
    jcasType.ll_cas.ll_setStringValue(addr, ((Disease_Type)jcasType).casFeatCode_time, v);}    
  }

    