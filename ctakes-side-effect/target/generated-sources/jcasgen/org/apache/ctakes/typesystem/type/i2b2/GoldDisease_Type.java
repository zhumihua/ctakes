
/* First created by JCasGen Tue Dec 09 23:28:38 EST 2014 */
package org.apache.ctakes.typesystem.type.i2b2;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Tue Dec 09 23:28:38 EST 2014
 * @generated */
public class GoldDisease_Type extends GoldTag_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (GoldDisease_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = GoldDisease_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new GoldDisease(addr, GoldDisease_Type.this);
  			   GoldDisease_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new GoldDisease(addr, GoldDisease_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = GoldDisease.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.apache.ctakes.typesystem.type.i2b2.GoldDisease");
 
  /** @generated */
  final Feature casFeat_indicator;
  /** @generated */
  final int     casFeatCode_indicator;
  /** @generated */ 
  public String getIndicator(int addr) {
        if (featOkTst && casFeat_indicator == null)
      jcas.throwFeatMissing("indicator", "org.apache.ctakes.typesystem.type.i2b2.GoldDisease");
    return ll_cas.ll_getStringValue(addr, casFeatCode_indicator);
  }
  /** @generated */    
  public void setIndicator(int addr, String v) {
        if (featOkTst && casFeat_indicator == null)
      jcas.throwFeatMissing("indicator", "org.apache.ctakes.typesystem.type.i2b2.GoldDisease");
    ll_cas.ll_setStringValue(addr, casFeatCode_indicator, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public GoldDisease_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_indicator = jcas.getRequiredFeatureDE(casType, "indicator", "uima.cas.String", featOkTst);
    casFeatCode_indicator  = (null == casFeat_indicator) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_indicator).getCode();

  }
}



    