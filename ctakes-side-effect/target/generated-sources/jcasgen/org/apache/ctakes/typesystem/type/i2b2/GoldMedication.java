

/* First created by JCasGen Tue Dec 09 23:28:38 EST 2014 */
package org.apache.ctakes.typesystem.type.i2b2;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Tue Dec 09 23:28:38 EST 2014
 * XML source: /Users/lusisi/i2b2/apache-ctakes-3.1.1-src/ctakes-side-effect/src/main/resources/org/apache/ctakes/sideeffect/types/TypeSystem.xml
 * @generated */
public class GoldMedication extends GoldTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(GoldMedication.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected GoldMedication() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public GoldMedication(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public GoldMedication(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public GoldMedication(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: type1

  /** getter for type1 - gets 
   * @generated */
  public String getType1() {
    if (GoldMedication_Type.featOkTst && ((GoldMedication_Type)jcasType).casFeat_type1 == null)
      jcasType.jcas.throwFeatMissing("type1", "org.apache.ctakes.typesystem.type.i2b2.GoldMedication");
    return jcasType.ll_cas.ll_getStringValue(addr, ((GoldMedication_Type)jcasType).casFeatCode_type1);}
    
  /** setter for type1 - sets  
   * @generated */
  public void setType1(String v) {
    if (GoldMedication_Type.featOkTst && ((GoldMedication_Type)jcasType).casFeat_type1 == null)
      jcasType.jcas.throwFeatMissing("type1", "org.apache.ctakes.typesystem.type.i2b2.GoldMedication");
    jcasType.ll_cas.ll_setStringValue(addr, ((GoldMedication_Type)jcasType).casFeatCode_type1, v);}    
   
    
  //*--------------*
  //* Feature: type2

  /** getter for type2 - gets 
   * @generated */
  public String getType2() {
    if (GoldMedication_Type.featOkTst && ((GoldMedication_Type)jcasType).casFeat_type2 == null)
      jcasType.jcas.throwFeatMissing("type2", "org.apache.ctakes.typesystem.type.i2b2.GoldMedication");
    return jcasType.ll_cas.ll_getStringValue(addr, ((GoldMedication_Type)jcasType).casFeatCode_type2);}
    
  /** setter for type2 - sets  
   * @generated */
  public void setType2(String v) {
    if (GoldMedication_Type.featOkTst && ((GoldMedication_Type)jcasType).casFeat_type2 == null)
      jcasType.jcas.throwFeatMissing("type2", "org.apache.ctakes.typesystem.type.i2b2.GoldMedication");
    jcasType.ll_cas.ll_setStringValue(addr, ((GoldMedication_Type)jcasType).casFeatCode_type2, v);}    
  }

    