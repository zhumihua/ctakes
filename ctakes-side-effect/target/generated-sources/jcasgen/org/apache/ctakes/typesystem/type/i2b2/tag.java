

/* First created by JCasGen Tue Dec 09 23:28:38 EST 2014 */
package org.apache.ctakes.typesystem.type.i2b2;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Tue Dec 09 23:28:38 EST 2014
 * XML source: /Users/lusisi/i2b2/apache-ctakes-3.1.1-src/ctakes-side-effect/src/main/resources/org/apache/ctakes/sideeffect/types/TypeSystem.xml
 * @generated */
public class tag extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(tag.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected tag() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public tag(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public tag(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public tag(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: indicatorName

  /** getter for indicatorName - gets the element name
   * @generated */
  public String getIndicatorName() {
    if (tag_Type.featOkTst && ((tag_Type)jcasType).casFeat_indicatorName == null)
      jcasType.jcas.throwFeatMissing("indicatorName", "org.apache.ctakes.typesystem.type.i2b2.tag");
    return jcasType.ll_cas.ll_getStringValue(addr, ((tag_Type)jcasType).casFeatCode_indicatorName);}
    
  /** setter for indicatorName - sets the element name 
   * @generated */
  public void setIndicatorName(String v) {
    if (tag_Type.featOkTst && ((tag_Type)jcasType).casFeat_indicatorName == null)
      jcasType.jcas.throwFeatMissing("indicatorName", "org.apache.ctakes.typesystem.type.i2b2.tag");
    jcasType.ll_cas.ll_setStringValue(addr, ((tag_Type)jcasType).casFeatCode_indicatorName, v);}    
  }

    