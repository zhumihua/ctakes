

/* First created by JCasGen Tue Dec 09 23:28:41 EST 2014 */
package org.apache.ctakes.smokingstatus.type.libsvm;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Tue Dec 09 23:28:41 EST 2014
 * XML source: /Users/lusisi/i2b2/apache-ctakes-3.1.1-src/ctakes-smoking-status/src/main/resources/org/apache/ctakes/smokingstatus/types/TypeSystem.xml
 * @generated */
public class DateAttributeValue extends AttributeValue {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(DateAttributeValue.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected DateAttributeValue() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public DateAttributeValue(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public DateAttributeValue(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public DateAttributeValue(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: dateValue

  /** getter for dateValue - gets 
   * @generated */
  public String getDateValue() {
    if (DateAttributeValue_Type.featOkTst && ((DateAttributeValue_Type)jcasType).casFeat_dateValue == null)
      jcasType.jcas.throwFeatMissing("dateValue", "org.apache.ctakes.smokingstatus.type.libsvm.DateAttributeValue");
    return jcasType.ll_cas.ll_getStringValue(addr, ((DateAttributeValue_Type)jcasType).casFeatCode_dateValue);}
    
  /** setter for dateValue - sets  
   * @generated */
  public void setDateValue(String v) {
    if (DateAttributeValue_Type.featOkTst && ((DateAttributeValue_Type)jcasType).casFeat_dateValue == null)
      jcasType.jcas.throwFeatMissing("dateValue", "org.apache.ctakes.smokingstatus.type.libsvm.DateAttributeValue");
    jcasType.ll_cas.ll_setStringValue(addr, ((DateAttributeValue_Type)jcasType).casFeatCode_dateValue, v);}    
  }

    