

/* First created by JCasGen Tue Dec 09 23:28:41 EST 2014 */
package org.apache.ctakes.smokingstatus.type.libsvm;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Tue Dec 09 23:28:41 EST 2014
 * XML source: /Users/lusisi/i2b2/apache-ctakes-3.1.1-src/ctakes-smoking-status/src/main/resources/org/apache/ctakes/smokingstatus/types/TypeSystem.xml
 * @generated */
public class NominalAttributeValue extends AttributeValue {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(NominalAttributeValue.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected NominalAttributeValue() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public NominalAttributeValue(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public NominalAttributeValue(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public NominalAttributeValue(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: nominalValue

  /** getter for nominalValue - gets 
   * @generated */
  public String getNominalValue() {
    if (NominalAttributeValue_Type.featOkTst && ((NominalAttributeValue_Type)jcasType).casFeat_nominalValue == null)
      jcasType.jcas.throwFeatMissing("nominalValue", "org.apache.ctakes.smokingstatus.type.libsvm.NominalAttributeValue");
    return jcasType.ll_cas.ll_getStringValue(addr, ((NominalAttributeValue_Type)jcasType).casFeatCode_nominalValue);}
    
  /** setter for nominalValue - sets  
   * @generated */
  public void setNominalValue(String v) {
    if (NominalAttributeValue_Type.featOkTst && ((NominalAttributeValue_Type)jcasType).casFeat_nominalValue == null)
      jcasType.jcas.throwFeatMissing("nominalValue", "org.apache.ctakes.smokingstatus.type.libsvm.NominalAttributeValue");
    jcasType.ll_cas.ll_setStringValue(addr, ((NominalAttributeValue_Type)jcasType).casFeatCode_nominalValue, v);}    
  }

    