

/* First created by JCasGen Tue Dec 09 23:28:41 EST 2014 */
package org.apache.ctakes.smokingstatus.type.libsvm;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** Corresponds to the ARFF numeric attributes.
 * Updated by JCasGen Tue Dec 09 23:28:41 EST 2014
 * XML source: /Users/lusisi/i2b2/apache-ctakes-3.1.1-src/ctakes-smoking-status/src/main/resources/org/apache/ctakes/smokingstatus/types/TypeSystem.xml
 * @generated */
public class NumericAttributeValue extends AttributeValue {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(NumericAttributeValue.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected NumericAttributeValue() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public NumericAttributeValue(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public NumericAttributeValue(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public NumericAttributeValue(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: numericValue

  /** getter for numericValue - gets 
   * @generated */
  public String getNumericValue() {
    if (NumericAttributeValue_Type.featOkTst && ((NumericAttributeValue_Type)jcasType).casFeat_numericValue == null)
      jcasType.jcas.throwFeatMissing("numericValue", "org.apache.ctakes.smokingstatus.type.libsvm.NumericAttributeValue");
    return jcasType.ll_cas.ll_getStringValue(addr, ((NumericAttributeValue_Type)jcasType).casFeatCode_numericValue);}
    
  /** setter for numericValue - sets  
   * @generated */
  public void setNumericValue(String v) {
    if (NumericAttributeValue_Type.featOkTst && ((NumericAttributeValue_Type)jcasType).casFeat_numericValue == null)
      jcasType.jcas.throwFeatMissing("numericValue", "org.apache.ctakes.smokingstatus.type.libsvm.NumericAttributeValue");
    jcasType.ll_cas.ll_setStringValue(addr, ((NumericAttributeValue_Type)jcasType).casFeatCode_numericValue, v);}    
  }

    