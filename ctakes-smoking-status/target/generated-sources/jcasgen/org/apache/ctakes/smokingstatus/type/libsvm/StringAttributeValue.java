

/* First created by JCasGen Tue Dec 09 23:28:41 EST 2014 */
package org.apache.ctakes.smokingstatus.type.libsvm;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Tue Dec 09 23:28:41 EST 2014
 * XML source: /Users/lusisi/i2b2/apache-ctakes-3.1.1-src/ctakes-smoking-status/src/main/resources/org/apache/ctakes/smokingstatus/types/TypeSystem.xml
 * @generated */
public class StringAttributeValue extends AttributeValue {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(StringAttributeValue.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected StringAttributeValue() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public StringAttributeValue(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public StringAttributeValue(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public StringAttributeValue(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: attributeStringValue

  /** getter for attributeStringValue - gets 
   * @generated */
  public String getAttributeStringValue() {
    if (StringAttributeValue_Type.featOkTst && ((StringAttributeValue_Type)jcasType).casFeat_attributeStringValue == null)
      jcasType.jcas.throwFeatMissing("attributeStringValue", "org.apache.ctakes.smokingstatus.type.libsvm.StringAttributeValue");
    return jcasType.ll_cas.ll_getStringValue(addr, ((StringAttributeValue_Type)jcasType).casFeatCode_attributeStringValue);}
    
  /** setter for attributeStringValue - sets  
   * @generated */
  public void setAttributeStringValue(String v) {
    if (StringAttributeValue_Type.featOkTst && ((StringAttributeValue_Type)jcasType).casFeat_attributeStringValue == null)
      jcasType.jcas.throwFeatMissing("attributeStringValue", "org.apache.ctakes.smokingstatus.type.libsvm.StringAttributeValue");
    jcasType.ll_cas.ll_setStringValue(addr, ((StringAttributeValue_Type)jcasType).casFeatCode_attributeStringValue, v);}    
  }

    