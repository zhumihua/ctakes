
/* First created by JCasGen Tue Dec 09 23:28:41 EST 2014 */
package org.apache.ctakes.smokingstatus.type.libsvm;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Tue Dec 09 23:28:41 EST 2014
 * @generated */
public class StringAttributeValue_Type extends AttributeValue_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (StringAttributeValue_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = StringAttributeValue_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new StringAttributeValue(addr, StringAttributeValue_Type.this);
  			   StringAttributeValue_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new StringAttributeValue(addr, StringAttributeValue_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = StringAttributeValue.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.apache.ctakes.smokingstatus.type.libsvm.StringAttributeValue");
 
  /** @generated */
  final Feature casFeat_attributeStringValue;
  /** @generated */
  final int     casFeatCode_attributeStringValue;
  /** @generated */ 
  public String getAttributeStringValue(int addr) {
        if (featOkTst && casFeat_attributeStringValue == null)
      jcas.throwFeatMissing("attributeStringValue", "org.apache.ctakes.smokingstatus.type.libsvm.StringAttributeValue");
    return ll_cas.ll_getStringValue(addr, casFeatCode_attributeStringValue);
  }
  /** @generated */    
  public void setAttributeStringValue(int addr, String v) {
        if (featOkTst && casFeat_attributeStringValue == null)
      jcas.throwFeatMissing("attributeStringValue", "org.apache.ctakes.smokingstatus.type.libsvm.StringAttributeValue");
    ll_cas.ll_setStringValue(addr, casFeatCode_attributeStringValue, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public StringAttributeValue_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_attributeStringValue = jcas.getRequiredFeatureDE(casType, "attributeStringValue", "uima.cas.String", featOkTst);
    casFeatCode_attributeStringValue  = (null == casFeat_attributeStringValue) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_attributeStringValue).getCode();

  }
}



    