
/* First created by JCasGen Tue Dec 09 23:28:41 EST 2014 */
package org.apache.ctakes.typesystem.type.i2b2;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** 
 * Updated by JCasGen Tue Dec 09 23:28:41 EST 2014
 * @generated */
public class GoldTag_Type extends Annotation_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (GoldTag_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = GoldTag_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new GoldTag(addr, GoldTag_Type.this);
  			   GoldTag_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new GoldTag(addr, GoldTag_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = GoldTag.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.apache.ctakes.typesystem.type.i2b2.GoldTag");
 
  /** @generated */
  final Feature casFeat_tagName;
  /** @generated */
  final int     casFeatCode_tagName;
  /** @generated */ 
  public String getTagName(int addr) {
        if (featOkTst && casFeat_tagName == null)
      jcas.throwFeatMissing("tagName", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    return ll_cas.ll_getStringValue(addr, casFeatCode_tagName);
  }
  /** @generated */    
  public void setTagName(int addr, String v) {
        if (featOkTst && casFeat_tagName == null)
      jcas.throwFeatMissing("tagName", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    ll_cas.ll_setStringValue(addr, casFeatCode_tagName, v);}
    
  
 
  /** @generated */
  final Feature casFeat_isMed;
  /** @generated */
  final int     casFeatCode_isMed;
  /** @generated */ 
  public boolean getIsMed(int addr) {
        if (featOkTst && casFeat_isMed == null)
      jcas.throwFeatMissing("isMed", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    return ll_cas.ll_getBooleanValue(addr, casFeatCode_isMed);
  }
  /** @generated */    
  public void setIsMed(int addr, boolean v) {
        if (featOkTst && casFeat_isMed == null)
      jcas.throwFeatMissing("isMed", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    ll_cas.ll_setBooleanValue(addr, casFeatCode_isMed, v);}
    
  
 
  /** @generated */
  final Feature casFeat_sectName;
  /** @generated */
  final int     casFeatCode_sectName;
  /** @generated */ 
  public String getSectName(int addr) {
        if (featOkTst && casFeat_sectName == null)
      jcas.throwFeatMissing("sectName", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    return ll_cas.ll_getStringValue(addr, casFeatCode_sectName);
  }
  /** @generated */    
  public void setSectName(int addr, String v) {
        if (featOkTst && casFeat_sectName == null)
      jcas.throwFeatMissing("sectName", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    ll_cas.ll_setStringValue(addr, casFeatCode_sectName, v);}
    
  
 
  /** @generated */
  final Feature casFeat_annoText;
  /** @generated */
  final int     casFeatCode_annoText;
  /** @generated */ 
  public String getAnnoText(int addr) {
        if (featOkTst && casFeat_annoText == null)
      jcas.throwFeatMissing("annoText", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    return ll_cas.ll_getStringValue(addr, casFeatCode_annoText);
  }
  /** @generated */    
  public void setAnnoText(int addr, String v) {
        if (featOkTst && casFeat_annoText == null)
      jcas.throwFeatMissing("annoText", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    ll_cas.ll_setStringValue(addr, casFeatCode_annoText, v);}
    
  
 
  /** @generated */
  final Feature casFeat_time_after;
  /** @generated */
  final int     casFeatCode_time_after;
  /** @generated */ 
  public int getTime_after(int addr) {
        if (featOkTst && casFeat_time_after == null)
      jcas.throwFeatMissing("time_after", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    return ll_cas.ll_getIntValue(addr, casFeatCode_time_after);
  }
  /** @generated */    
  public void setTime_after(int addr, int v) {
        if (featOkTst && casFeat_time_after == null)
      jcas.throwFeatMissing("time_after", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    ll_cas.ll_setIntValue(addr, casFeatCode_time_after, v);}
    
  
 
  /** @generated */
  final Feature casFeat_time_before;
  /** @generated */
  final int     casFeatCode_time_before;
  /** @generated */ 
  public int getTime_before(int addr) {
        if (featOkTst && casFeat_time_before == null)
      jcas.throwFeatMissing("time_before", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    return ll_cas.ll_getIntValue(addr, casFeatCode_time_before);
  }
  /** @generated */    
  public void setTime_before(int addr, int v) {
        if (featOkTst && casFeat_time_before == null)
      jcas.throwFeatMissing("time_before", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    ll_cas.ll_setIntValue(addr, casFeatCode_time_before, v);}
    
  
 
  /** @generated */
  final Feature casFeat_time_during;
  /** @generated */
  final int     casFeatCode_time_during;
  /** @generated */ 
  public int getTime_during(int addr) {
        if (featOkTst && casFeat_time_during == null)
      jcas.throwFeatMissing("time_during", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    return ll_cas.ll_getIntValue(addr, casFeatCode_time_during);
  }
  /** @generated */    
  public void setTime_during(int addr, int v) {
        if (featOkTst && casFeat_time_during == null)
      jcas.throwFeatMissing("time_during", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    ll_cas.ll_setIntValue(addr, casFeatCode_time_during, v);}
    
  
 
  /** @generated */
  final Feature casFeat_time_value;
  /** @generated */
  final int     casFeatCode_time_value;
  /** @generated */ 
  public int getTime_value(int addr) {
        if (featOkTst && casFeat_time_value == null)
      jcas.throwFeatMissing("time_value", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    return ll_cas.ll_getIntValue(addr, casFeatCode_time_value);
  }
  /** @generated */    
  public void setTime_value(int addr, int v) {
        if (featOkTst && casFeat_time_value == null)
      jcas.throwFeatMissing("time_value", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    ll_cas.ll_setIntValue(addr, casFeatCode_time_value, v);}
    
  
 
  /** @generated */
  final Feature casFeat_dct;
  /** @generated */
  final int     casFeatCode_dct;
  /** @generated */ 
  public String getDct(int addr) {
        if (featOkTst && casFeat_dct == null)
      jcas.throwFeatMissing("dct", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    return ll_cas.ll_getStringValue(addr, casFeatCode_dct);
  }
  /** @generated */    
  public void setDct(int addr, String v) {
        if (featOkTst && casFeat_dct == null)
      jcas.throwFeatMissing("dct", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    ll_cas.ll_setStringValue(addr, casFeatCode_dct, v);}
    
  
 
  /** @generated */
  final Feature casFeat_annoStart;
  /** @generated */
  final int     casFeatCode_annoStart;
  /** @generated */ 
  public int getAnnoStart(int addr) {
        if (featOkTst && casFeat_annoStart == null)
      jcas.throwFeatMissing("annoStart", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    return ll_cas.ll_getIntValue(addr, casFeatCode_annoStart);
  }
  /** @generated */    
  public void setAnnoStart(int addr, int v) {
        if (featOkTst && casFeat_annoStart == null)
      jcas.throwFeatMissing("annoStart", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    ll_cas.ll_setIntValue(addr, casFeatCode_annoStart, v);}
    
  
 
  /** @generated */
  final Feature casFeat_annoEnd;
  /** @generated */
  final int     casFeatCode_annoEnd;
  /** @generated */ 
  public int getAnnoEnd(int addr) {
        if (featOkTst && casFeat_annoEnd == null)
      jcas.throwFeatMissing("annoEnd", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    return ll_cas.ll_getIntValue(addr, casFeatCode_annoEnd);
  }
  /** @generated */    
  public void setAnnoEnd(int addr, int v) {
        if (featOkTst && casFeat_annoEnd == null)
      jcas.throwFeatMissing("annoEnd", "org.apache.ctakes.typesystem.type.i2b2.GoldTag");
    ll_cas.ll_setIntValue(addr, casFeatCode_annoEnd, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public GoldTag_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_tagName = jcas.getRequiredFeatureDE(casType, "tagName", "uima.cas.String", featOkTst);
    casFeatCode_tagName  = (null == casFeat_tagName) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_tagName).getCode();

 
    casFeat_isMed = jcas.getRequiredFeatureDE(casType, "isMed", "uima.cas.Boolean", featOkTst);
    casFeatCode_isMed  = (null == casFeat_isMed) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_isMed).getCode();

 
    casFeat_sectName = jcas.getRequiredFeatureDE(casType, "sectName", "uima.cas.String", featOkTst);
    casFeatCode_sectName  = (null == casFeat_sectName) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_sectName).getCode();

 
    casFeat_annoText = jcas.getRequiredFeatureDE(casType, "annoText", "uima.cas.String", featOkTst);
    casFeatCode_annoText  = (null == casFeat_annoText) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_annoText).getCode();

 
    casFeat_time_after = jcas.getRequiredFeatureDE(casType, "time_after", "uima.cas.Integer", featOkTst);
    casFeatCode_time_after  = (null == casFeat_time_after) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_time_after).getCode();

 
    casFeat_time_before = jcas.getRequiredFeatureDE(casType, "time_before", "uima.cas.Integer", featOkTst);
    casFeatCode_time_before  = (null == casFeat_time_before) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_time_before).getCode();

 
    casFeat_time_during = jcas.getRequiredFeatureDE(casType, "time_during", "uima.cas.Integer", featOkTst);
    casFeatCode_time_during  = (null == casFeat_time_during) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_time_during).getCode();

 
    casFeat_time_value = jcas.getRequiredFeatureDE(casType, "time_value", "uima.cas.Integer", featOkTst);
    casFeatCode_time_value  = (null == casFeat_time_value) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_time_value).getCode();

 
    casFeat_dct = jcas.getRequiredFeatureDE(casType, "dct", "uima.cas.String", featOkTst);
    casFeatCode_dct  = (null == casFeat_dct) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_dct).getCode();

 
    casFeat_annoStart = jcas.getRequiredFeatureDE(casType, "annoStart", "uima.cas.Integer", featOkTst);
    casFeatCode_annoStart  = (null == casFeat_annoStart) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_annoStart).getCode();

 
    casFeat_annoEnd = jcas.getRequiredFeatureDE(casType, "annoEnd", "uima.cas.Integer", featOkTst);
    casFeatCode_annoEnd  = (null == casFeat_annoEnd) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_annoEnd).getCode();

  }
}



    