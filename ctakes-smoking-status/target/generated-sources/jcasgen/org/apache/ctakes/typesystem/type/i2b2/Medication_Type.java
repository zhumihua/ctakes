
/* First created by JCasGen Tue Dec 09 23:28:41 EST 2014 */
package org.apache.ctakes.typesystem.type.i2b2;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Tue Dec 09 23:28:41 EST 2014
 * @generated */
public class Medication_Type extends tag_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Medication_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Medication_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Medication(addr, Medication_Type.this);
  			   Medication_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Medication(addr, Medication_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Medication.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.apache.ctakes.typesystem.type.i2b2.Medication");
 
  /** @generated */
  final Feature casFeat_type1;
  /** @generated */
  final int     casFeatCode_type1;
  /** @generated */ 
  public String getType1(int addr) {
        if (featOkTst && casFeat_type1 == null)
      jcas.throwFeatMissing("type1", "org.apache.ctakes.typesystem.type.i2b2.Medication");
    return ll_cas.ll_getStringValue(addr, casFeatCode_type1);
  }
  /** @generated */    
  public void setType1(int addr, String v) {
        if (featOkTst && casFeat_type1 == null)
      jcas.throwFeatMissing("type1", "org.apache.ctakes.typesystem.type.i2b2.Medication");
    ll_cas.ll_setStringValue(addr, casFeatCode_type1, v);}
    
  
 
  /** @generated */
  final Feature casFeat_type2;
  /** @generated */
  final int     casFeatCode_type2;
  /** @generated */ 
  public String getType2(int addr) {
        if (featOkTst && casFeat_type2 == null)
      jcas.throwFeatMissing("type2", "org.apache.ctakes.typesystem.type.i2b2.Medication");
    return ll_cas.ll_getStringValue(addr, casFeatCode_type2);
  }
  /** @generated */    
  public void setType2(int addr, String v) {
        if (featOkTst && casFeat_type2 == null)
      jcas.throwFeatMissing("type2", "org.apache.ctakes.typesystem.type.i2b2.Medication");
    ll_cas.ll_setStringValue(addr, casFeatCode_type2, v);}
    
  
 
  /** @generated */
  final Feature casFeat_time;
  /** @generated */
  final int     casFeatCode_time;
  /** @generated */ 
  public String getTime(int addr) {
        if (featOkTst && casFeat_time == null)
      jcas.throwFeatMissing("time", "org.apache.ctakes.typesystem.type.i2b2.Medication");
    return ll_cas.ll_getStringValue(addr, casFeatCode_time);
  }
  /** @generated */    
  public void setTime(int addr, String v) {
        if (featOkTst && casFeat_time == null)
      jcas.throwFeatMissing("time", "org.apache.ctakes.typesystem.type.i2b2.Medication");
    ll_cas.ll_setStringValue(addr, casFeatCode_time, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public Medication_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_type1 = jcas.getRequiredFeatureDE(casType, "type1", "uima.cas.String", featOkTst);
    casFeatCode_type1  = (null == casFeat_type1) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_type1).getCode();

 
    casFeat_type2 = jcas.getRequiredFeatureDE(casType, "type2", "uima.cas.String", featOkTst);
    casFeatCode_type2  = (null == casFeat_type2) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_type2).getCode();

 
    casFeat_time = jcas.getRequiredFeatureDE(casType, "time", "uima.cas.String", featOkTst);
    casFeatCode_time  = (null == casFeat_time) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_time).getCode();

  }
}



    