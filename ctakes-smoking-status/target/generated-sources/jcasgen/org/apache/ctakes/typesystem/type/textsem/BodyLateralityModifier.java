

/* First created by JCasGen Tue Dec 09 23:28:41 EST 2014 */
package org.apache.ctakes.typesystem.type.textsem;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** The proximity of the location in anatomical terms
				(distal, proximal, superior, anterior and etc.). This is
				finer-grained to allow combinations of values.
 * Updated by JCasGen Tue Dec 09 23:28:41 EST 2014
 * XML source: /Users/lusisi/i2b2/apache-ctakes-3.1.1-src/ctakes-smoking-status/src/main/resources/org/apache/ctakes/smokingstatus/types/TypeSystem.xml
 * @generated */
public class BodyLateralityModifier extends Modifier {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(BodyLateralityModifier.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected BodyLateralityModifier() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public BodyLateralityModifier(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public BodyLateralityModifier(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public BodyLateralityModifier(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
}

    