

/* First created by JCasGen Thu Dec 11 10:42:01 EST 2014 */
package org.apache.ctakes.medtime.type;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.ctakes.typesystem.type.textspan.Sentence;
import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Thu Dec 11 10:42:01 EST 2014
 * XML source: /Users/lusisi/i2b2/apache-ctakes-3.1.1-src/ctakes-type-system/src/main/resources/org/apache/ctakes/typesystem/types/TypeSystem.xml
 * @generated */
public class MedTimex3 extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(MedTimex3.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected MedTimex3() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public MedTimex3(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public MedTimex3(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public MedTimex3(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: filename

  /** getter for filename - gets 
   * @generated */
  public String getFilename() {
    if (MedTimex3_Type.featOkTst && ((MedTimex3_Type)jcasType).casFeat_filename == null)
      jcasType.jcas.throwFeatMissing("filename", "org.apache.ctakes.medtime.type.MedTimex3");
    return jcasType.ll_cas.ll_getStringValue(addr, ((MedTimex3_Type)jcasType).casFeatCode_filename);}
    
  /** setter for filename - sets  
   * @generated */
  public void setFilename(String v) {
    if (MedTimex3_Type.featOkTst && ((MedTimex3_Type)jcasType).casFeat_filename == null)
      jcasType.jcas.throwFeatMissing("filename", "org.apache.ctakes.medtime.type.MedTimex3");
    jcasType.ll_cas.ll_setStringValue(addr, ((MedTimex3_Type)jcasType).casFeatCode_filename, v);}    
   
    
  //*--------------*
  //* Feature: firstTokId

  /** getter for firstTokId - gets 
   * @generated */
  public int getFirstTokId() {
    if (MedTimex3_Type.featOkTst && ((MedTimex3_Type)jcasType).casFeat_firstTokId == null)
      jcasType.jcas.throwFeatMissing("firstTokId", "org.apache.ctakes.medtime.type.MedTimex3");
    return jcasType.ll_cas.ll_getIntValue(addr, ((MedTimex3_Type)jcasType).casFeatCode_firstTokId);}
    
  /** setter for firstTokId - sets  
   * @generated */
  public void setFirstTokId(int v) {
    if (MedTimex3_Type.featOkTst && ((MedTimex3_Type)jcasType).casFeat_firstTokId == null)
      jcasType.jcas.throwFeatMissing("firstTokId", "org.apache.ctakes.medtime.type.MedTimex3");
    jcasType.ll_cas.ll_setIntValue(addr, ((MedTimex3_Type)jcasType).casFeatCode_firstTokId, v);}    
   
    
  //*--------------*
  //* Feature: allTokIds

  /** getter for allTokIds - gets 
   * @generated */
  public String getAllTokIds() {
    if (MedTimex3_Type.featOkTst && ((MedTimex3_Type)jcasType).casFeat_allTokIds == null)
      jcasType.jcas.throwFeatMissing("allTokIds", "org.apache.ctakes.medtime.type.MedTimex3");
    return jcasType.ll_cas.ll_getStringValue(addr, ((MedTimex3_Type)jcasType).casFeatCode_allTokIds);}
    
  /** setter for allTokIds - sets  
   * @generated */
  public void setAllTokIds(String v) {
    if (MedTimex3_Type.featOkTst && ((MedTimex3_Type)jcasType).casFeat_allTokIds == null)
      jcasType.jcas.throwFeatMissing("allTokIds", "org.apache.ctakes.medtime.type.MedTimex3");
    jcasType.ll_cas.ll_setStringValue(addr, ((MedTimex3_Type)jcasType).casFeatCode_allTokIds, v);}    
   
    
  //*--------------*
  //* Feature: timexId

  /** getter for timexId - gets 
   * @generated */
  public String getTimexId() {
    if (MedTimex3_Type.featOkTst && ((MedTimex3_Type)jcasType).casFeat_timexId == null)
      jcasType.jcas.throwFeatMissing("timexId", "org.apache.ctakes.medtime.type.MedTimex3");
    return jcasType.ll_cas.ll_getStringValue(addr, ((MedTimex3_Type)jcasType).casFeatCode_timexId);}
    
  /** setter for timexId - sets  
   * @generated */
  public void setTimexId(String v) {
    if (MedTimex3_Type.featOkTst && ((MedTimex3_Type)jcasType).casFeat_timexId == null)
      jcasType.jcas.throwFeatMissing("timexId", "org.apache.ctakes.medtime.type.MedTimex3");
    jcasType.ll_cas.ll_setStringValue(addr, ((MedTimex3_Type)jcasType).casFeatCode_timexId, v);}    
   
    
  //*--------------*
  //* Feature: timexInstance

  /** getter for timexInstance - gets 
   * @generated */
  public int getTimexInstance() {
    if (MedTimex3_Type.featOkTst && ((MedTimex3_Type)jcasType).casFeat_timexInstance == null)
      jcasType.jcas.throwFeatMissing("timexInstance", "org.apache.ctakes.medtime.type.MedTimex3");
    return jcasType.ll_cas.ll_getIntValue(addr, ((MedTimex3_Type)jcasType).casFeatCode_timexInstance);}
    
  /** setter for timexInstance - sets  
   * @generated */
  public void setTimexInstance(int v) {
    if (MedTimex3_Type.featOkTst && ((MedTimex3_Type)jcasType).casFeat_timexInstance == null)
      jcasType.jcas.throwFeatMissing("timexInstance", "org.apache.ctakes.medtime.type.MedTimex3");
    jcasType.ll_cas.ll_setIntValue(addr, ((MedTimex3_Type)jcasType).casFeatCode_timexInstance, v);}    
   
    
  //*--------------*
  //* Feature: contextSentence

  /** getter for contextSentence - gets 
   * @generated */
  public Sentence getContextSentence() {
    if (MedTimex3_Type.featOkTst && ((MedTimex3_Type)jcasType).casFeat_contextSentence == null)
      jcasType.jcas.throwFeatMissing("contextSentence", "org.apache.ctakes.medtime.type.MedTimex3");
    return (Sentence)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((MedTimex3_Type)jcasType).casFeatCode_contextSentence)));}
    
  /** setter for contextSentence - sets  
   * @generated */
  public void setContextSentence(Sentence v) {
    if (MedTimex3_Type.featOkTst && ((MedTimex3_Type)jcasType).casFeat_contextSentence == null)
      jcasType.jcas.throwFeatMissing("contextSentence", "org.apache.ctakes.medtime.type.MedTimex3");
    jcasType.ll_cas.ll_setRefValue(addr, ((MedTimex3_Type)jcasType).casFeatCode_contextSentence, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: timexType

  /** getter for timexType - gets 
   * @generated */
  public String getTimexType() {
    if (MedTimex3_Type.featOkTst && ((MedTimex3_Type)jcasType).casFeat_timexType == null)
      jcasType.jcas.throwFeatMissing("timexType", "org.apache.ctakes.medtime.type.MedTimex3");
    return jcasType.ll_cas.ll_getStringValue(addr, ((MedTimex3_Type)jcasType).casFeatCode_timexType);}
    
  /** setter for timexType - sets  
   * @generated */
  public void setTimexType(String v) {
    if (MedTimex3_Type.featOkTst && ((MedTimex3_Type)jcasType).casFeat_timexType == null)
      jcasType.jcas.throwFeatMissing("timexType", "org.apache.ctakes.medtime.type.MedTimex3");
    jcasType.ll_cas.ll_setStringValue(addr, ((MedTimex3_Type)jcasType).casFeatCode_timexType, v);}    
   
    
  //*--------------*
  //* Feature: timexValue

  /** getter for timexValue - gets 
   * @generated */
  public String getTimexValue() {
    if (MedTimex3_Type.featOkTst && ((MedTimex3_Type)jcasType).casFeat_timexValue == null)
      jcasType.jcas.throwFeatMissing("timexValue", "org.apache.ctakes.medtime.type.MedTimex3");
    return jcasType.ll_cas.ll_getStringValue(addr, ((MedTimex3_Type)jcasType).casFeatCode_timexValue);}
    
  /** setter for timexValue - sets  
   * @generated */
  public void setTimexValue(String v) {
    if (MedTimex3_Type.featOkTst && ((MedTimex3_Type)jcasType).casFeat_timexValue == null)
      jcasType.jcas.throwFeatMissing("timexValue", "org.apache.ctakes.medtime.type.MedTimex3");
    jcasType.ll_cas.ll_setStringValue(addr, ((MedTimex3_Type)jcasType).casFeatCode_timexValue, v);}    
   
    
  //*--------------*
  //* Feature: foundByRule

  /** getter for foundByRule - gets 
   * @generated */
  public String getFoundByRule() {
    if (MedTimex3_Type.featOkTst && ((MedTimex3_Type)jcasType).casFeat_foundByRule == null)
      jcasType.jcas.throwFeatMissing("foundByRule", "org.apache.ctakes.medtime.type.MedTimex3");
    return jcasType.ll_cas.ll_getStringValue(addr, ((MedTimex3_Type)jcasType).casFeatCode_foundByRule);}
    
  /** setter for foundByRule - sets  
   * @generated */
  public void setFoundByRule(String v) {
    if (MedTimex3_Type.featOkTst && ((MedTimex3_Type)jcasType).casFeat_foundByRule == null)
      jcasType.jcas.throwFeatMissing("foundByRule", "org.apache.ctakes.medtime.type.MedTimex3");
    jcasType.ll_cas.ll_setStringValue(addr, ((MedTimex3_Type)jcasType).casFeatCode_foundByRule, v);}    
   
    
  //*--------------*
  //* Feature: timexQuant

  /** getter for timexQuant - gets 
   * @generated */
  public String getTimexQuant() {
    if (MedTimex3_Type.featOkTst && ((MedTimex3_Type)jcasType).casFeat_timexQuant == null)
      jcasType.jcas.throwFeatMissing("timexQuant", "org.apache.ctakes.medtime.type.MedTimex3");
    return jcasType.ll_cas.ll_getStringValue(addr, ((MedTimex3_Type)jcasType).casFeatCode_timexQuant);}
    
  /** setter for timexQuant - sets  
   * @generated */
  public void setTimexQuant(String v) {
    if (MedTimex3_Type.featOkTst && ((MedTimex3_Type)jcasType).casFeat_timexQuant == null)
      jcasType.jcas.throwFeatMissing("timexQuant", "org.apache.ctakes.medtime.type.MedTimex3");
    jcasType.ll_cas.ll_setStringValue(addr, ((MedTimex3_Type)jcasType).casFeatCode_timexQuant, v);}    
   
    
  //*--------------*
  //* Feature: timexFreq

  /** getter for timexFreq - gets 
   * @generated */
  public String getTimexFreq() {
    if (MedTimex3_Type.featOkTst && ((MedTimex3_Type)jcasType).casFeat_timexFreq == null)
      jcasType.jcas.throwFeatMissing("timexFreq", "org.apache.ctakes.medtime.type.MedTimex3");
    return jcasType.ll_cas.ll_getStringValue(addr, ((MedTimex3_Type)jcasType).casFeatCode_timexFreq);}
    
  /** setter for timexFreq - sets  
   * @generated */
  public void setTimexFreq(String v) {
    if (MedTimex3_Type.featOkTst && ((MedTimex3_Type)jcasType).casFeat_timexFreq == null)
      jcasType.jcas.throwFeatMissing("timexFreq", "org.apache.ctakes.medtime.type.MedTimex3");
    jcasType.ll_cas.ll_setStringValue(addr, ((MedTimex3_Type)jcasType).casFeatCode_timexFreq, v);}    
   
    
  //*--------------*
  //* Feature: timexMod

  /** getter for timexMod - gets 
   * @generated */
  public String getTimexMod() {
    if (MedTimex3_Type.featOkTst && ((MedTimex3_Type)jcasType).casFeat_timexMod == null)
      jcasType.jcas.throwFeatMissing("timexMod", "org.apache.ctakes.medtime.type.MedTimex3");
    return jcasType.ll_cas.ll_getStringValue(addr, ((MedTimex3_Type)jcasType).casFeatCode_timexMod);}
    
  /** setter for timexMod - sets  
   * @generated */
  public void setTimexMod(String v) {
    if (MedTimex3_Type.featOkTst && ((MedTimex3_Type)jcasType).casFeat_timexMod == null)
      jcasType.jcas.throwFeatMissing("timexMod", "org.apache.ctakes.medtime.type.MedTimex3");
    jcasType.ll_cas.ll_setStringValue(addr, ((MedTimex3_Type)jcasType).casFeatCode_timexMod, v);}    
  }

    