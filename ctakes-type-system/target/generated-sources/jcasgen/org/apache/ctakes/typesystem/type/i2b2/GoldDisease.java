

/* First created by JCasGen Thu Dec 11 10:42:01 EST 2014 */
package org.apache.ctakes.typesystem.type.i2b2;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Thu Dec 11 10:42:01 EST 2014
 * XML source: /Users/lusisi/i2b2/apache-ctakes-3.1.1-src/ctakes-type-system/src/main/resources/org/apache/ctakes/typesystem/types/TypeSystem.xml
 * @generated */
public class GoldDisease extends GoldTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(GoldDisease.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected GoldDisease() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public GoldDisease(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public GoldDisease(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public GoldDisease(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: indicator

  /** getter for indicator - gets 
   * @generated */
  public String getIndicator() {
    if (GoldDisease_Type.featOkTst && ((GoldDisease_Type)jcasType).casFeat_indicator == null)
      jcasType.jcas.throwFeatMissing("indicator", "org.apache.ctakes.typesystem.type.i2b2.GoldDisease");
    return jcasType.ll_cas.ll_getStringValue(addr, ((GoldDisease_Type)jcasType).casFeatCode_indicator);}
    
  /** setter for indicator - sets  
   * @generated */
  public void setIndicator(String v) {
    if (GoldDisease_Type.featOkTst && ((GoldDisease_Type)jcasType).casFeat_indicator == null)
      jcasType.jcas.throwFeatMissing("indicator", "org.apache.ctakes.typesystem.type.i2b2.GoldDisease");
    jcasType.ll_cas.ll_setStringValue(addr, ((GoldDisease_Type)jcasType).casFeatCode_indicator, v);}    
  }

    