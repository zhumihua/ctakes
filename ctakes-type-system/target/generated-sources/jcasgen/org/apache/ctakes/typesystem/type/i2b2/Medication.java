

/* First created by JCasGen Thu Dec 11 10:42:01 EST 2014 */
package org.apache.ctakes.typesystem.type.i2b2;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Thu Dec 11 10:42:01 EST 2014
 * XML source: /Users/lusisi/i2b2/apache-ctakes-3.1.1-src/ctakes-type-system/src/main/resources/org/apache/ctakes/typesystem/types/TypeSystem.xml
 * @generated */
public class Medication extends tag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Medication.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Medication() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Medication(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Medication(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Medication(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: type1

  /** getter for type1 - gets 
   * @generated */
  public String getType1() {
    if (Medication_Type.featOkTst && ((Medication_Type)jcasType).casFeat_type1 == null)
      jcasType.jcas.throwFeatMissing("type1", "org.apache.ctakes.typesystem.type.i2b2.Medication");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Medication_Type)jcasType).casFeatCode_type1);}
    
  /** setter for type1 - sets  
   * @generated */
  public void setType1(String v) {
    if (Medication_Type.featOkTst && ((Medication_Type)jcasType).casFeat_type1 == null)
      jcasType.jcas.throwFeatMissing("type1", "org.apache.ctakes.typesystem.type.i2b2.Medication");
    jcasType.ll_cas.ll_setStringValue(addr, ((Medication_Type)jcasType).casFeatCode_type1, v);}    
   
    
  //*--------------*
  //* Feature: type2

  /** getter for type2 - gets 
   * @generated */
  public String getType2() {
    if (Medication_Type.featOkTst && ((Medication_Type)jcasType).casFeat_type2 == null)
      jcasType.jcas.throwFeatMissing("type2", "org.apache.ctakes.typesystem.type.i2b2.Medication");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Medication_Type)jcasType).casFeatCode_type2);}
    
  /** setter for type2 - sets  
   * @generated */
  public void setType2(String v) {
    if (Medication_Type.featOkTst && ((Medication_Type)jcasType).casFeat_type2 == null)
      jcasType.jcas.throwFeatMissing("type2", "org.apache.ctakes.typesystem.type.i2b2.Medication");
    jcasType.ll_cas.ll_setStringValue(addr, ((Medication_Type)jcasType).casFeatCode_type2, v);}    
   
    
  //*--------------*
  //* Feature: time

  /** getter for time - gets 
   * @generated */
  public String getTime() {
    if (Medication_Type.featOkTst && ((Medication_Type)jcasType).casFeat_time == null)
      jcasType.jcas.throwFeatMissing("time", "org.apache.ctakes.typesystem.type.i2b2.Medication");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Medication_Type)jcasType).casFeatCode_time);}
    
  /** setter for time - sets  
   * @generated */
  public void setTime(String v) {
    if (Medication_Type.featOkTst && ((Medication_Type)jcasType).casFeat_time == null)
      jcasType.jcas.throwFeatMissing("time", "org.apache.ctakes.typesystem.type.i2b2.Medication");
    jcasType.ll_cas.ll_setStringValue(addr, ((Medication_Type)jcasType).casFeatCode_time, v);}    
  }

    