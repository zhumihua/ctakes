
/* First created by JCasGen Thu Dec 11 10:42:01 EST 2014 */
package org.apache.ctakes.typesystem.type.i2b2;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** 
 * Updated by JCasGen Thu Dec 11 10:42:01 EST 2014
 * @generated */
public class Section_Type extends Annotation_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Section_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Section_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Section(addr, Section_Type.this);
  			   Section_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Section(addr, Section_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Section.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.apache.ctakes.typesystem.type.i2b2.Section");
 
  /** @generated */
  final Feature casFeat_sectName;
  /** @generated */
  final int     casFeatCode_sectName;
  /** @generated */ 
  public String getSectName(int addr) {
        if (featOkTst && casFeat_sectName == null)
      jcas.throwFeatMissing("sectName", "org.apache.ctakes.typesystem.type.i2b2.Section");
    return ll_cas.ll_getStringValue(addr, casFeatCode_sectName);
  }
  /** @generated */    
  public void setSectName(int addr, String v) {
        if (featOkTst && casFeat_sectName == null)
      jcas.throwFeatMissing("sectName", "org.apache.ctakes.typesystem.type.i2b2.Section");
    ll_cas.ll_setStringValue(addr, casFeatCode_sectName, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public Section_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_sectName = jcas.getRequiredFeatureDE(casType, "sectName", "uima.cas.String", featOkTst);
    casFeatCode_sectName  = (null == casFeat_sectName) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_sectName).getCode();

  }
}



    