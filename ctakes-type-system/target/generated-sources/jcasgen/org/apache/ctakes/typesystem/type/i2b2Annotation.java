

/* First created by JCasGen Wed Jun 25 03:07:52 PDT 2014 */
package org.apache.ctakes.typesystem.type;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Wed Jun 25 03:07:52 PDT 2014
 * XML source: /Users/lusisi/i2b2/apache-ctakes-3.1.1-src/ctakes-type-system/src/main/resources/org/apache/ctakes/typesystem/types/TypeSystem.xml
 * @generated */
public class i2b2Annotation extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(i2b2Annotation.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected i2b2Annotation() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public i2b2Annotation(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public i2b2Annotation(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public i2b2Annotation(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: indicatorName

  /** getter for indicatorName - gets the element name
   * @generated */
  public String getIndicatorName() {
    if (i2b2Annotation_Type.featOkTst && ((i2b2Annotation_Type)jcasType).casFeat_indicatorName == null)
      jcasType.jcas.throwFeatMissing("indicatorName", "org.apache.ctakes.typesystem.type.i2b2Annotation");
    return jcasType.ll_cas.ll_getStringValue(addr, ((i2b2Annotation_Type)jcasType).casFeatCode_indicatorName);}
    
  /** setter for indicatorName - sets the element name 
   * @generated */
  public void setIndicatorName(String v) {
    if (i2b2Annotation_Type.featOkTst && ((i2b2Annotation_Type)jcasType).casFeat_indicatorName == null)
      jcasType.jcas.throwFeatMissing("indicatorName", "org.apache.ctakes.typesystem.type.i2b2Annotation");
    jcasType.ll_cas.ll_setStringValue(addr, ((i2b2Annotation_Type)jcasType).casFeatCode_indicatorName, v);}    
  }

    