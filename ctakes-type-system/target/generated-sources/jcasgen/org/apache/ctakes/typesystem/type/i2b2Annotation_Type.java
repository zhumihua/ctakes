
/* First created by JCasGen Wed Jun 25 03:07:52 PDT 2014 */
package org.apache.ctakes.typesystem.type;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** 
 * Updated by JCasGen Wed Jun 25 03:07:52 PDT 2014
 * @generated */
public class i2b2Annotation_Type extends Annotation_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (i2b2Annotation_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = i2b2Annotation_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new i2b2Annotation(addr, i2b2Annotation_Type.this);
  			   i2b2Annotation_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new i2b2Annotation(addr, i2b2Annotation_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = i2b2Annotation.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.apache.ctakes.typesystem.type.i2b2Annotation");
 
  /** @generated */
  final Feature casFeat_indicatorName;
  /** @generated */
  final int     casFeatCode_indicatorName;
  /** @generated */ 
  public String getIndicatorName(int addr) {
        if (featOkTst && casFeat_indicatorName == null)
      jcas.throwFeatMissing("indicatorName", "org.apache.ctakes.typesystem.type.i2b2Annotation");
    return ll_cas.ll_getStringValue(addr, casFeatCode_indicatorName);
  }
  /** @generated */    
  public void setIndicatorName(int addr, String v) {
        if (featOkTst && casFeat_indicatorName == null)
      jcas.throwFeatMissing("indicatorName", "org.apache.ctakes.typesystem.type.i2b2Annotation");
    ll_cas.ll_setStringValue(addr, casFeatCode_indicatorName, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public i2b2Annotation_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_indicatorName = jcas.getRequiredFeatureDE(casType, "indicatorName", "uima.cas.String", featOkTst);
    casFeatCode_indicatorName  = (null == casFeat_indicatorName) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_indicatorName).getCode();

  }
}



    